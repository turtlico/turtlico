=======
Plugins
=======

Turtlico is extensible by plugins that can add extra icons.
For example the integrated RPi plugin adds commands to control GPIO via the gpiozero library.

Activating and deactivating plugins is done in Project properties.

You can open the plugins folder by clicking *Open user plugins folder* button in Project properties.
There you can put your additional plugins.

Plugin loading
==============

Turtlico stores default plugins in the app data folder (in Flatpak it is /app/share/turtlico/turtlico/plugins).

Installabe plugins should be stored in user plugins folder that is located in user data directory under "turtlico/turtlico/plugins" subfolder.
You can access this directory via Project properties dialog.

Plugin folder
=============
Every plugin folder contains one or more Python plugin files.

There is a one or multiple Python files. The names of these defines IDs of the plugins.

**Basic plugin folder structure**

| plugins
| └── [plugin id]
|     ├── [plugin id].py
|     └── icons
|         └── [SVG icon files]


Format of plugin files
======================

**Basic structure**

Plugin files are Python files that contains a get_plugin function. This function returns a Plugin object.

Creating plugin objects is described in `turtlico.lib <./lib/turtlico.lib.html#turtlico.lib.command.Plugin>`__ module documentation.

**Example plugin template**

.. code-block:: python
    :force:

    # flake8: noqa
    from turtlico.lib import Plugin, CommandCategory
    from turtlico.lib import CommandDefinition, CommandType, CommandModule, CommandEvent
    from turtlico.lib.icon import icon, text

    def get_plugin():
        p = Plugin('Example plugin', doc_url='https://example-plugin.org/docs.html')
        p.categories = [
            CommandCategory(p, text('🐢'), [
                CommandDefinition(
                    'hello', icon('hello.svg'), 'Hello', CommandType.METHOD, 'example.hello', '"World"'
                )
            ])
        ]
        p.modules = {
            'example_plugin': CommandModule(
                deps=(),
                code='import example'
            )
        }
        return p
