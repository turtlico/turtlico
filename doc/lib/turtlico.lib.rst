turtlico.lib package
====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   turtlico.lib.icon

Submodules
----------

turtlico.lib.codepiece module
-----------------------------

.. automodule:: turtlico.lib.codepiece
   :members:
   :undoc-members:
   :show-inheritance:

turtlico.lib.codepieceselection module
--------------------------------------

.. automodule:: turtlico.lib.codepieceselection
   :members:
   :undoc-members:
   :show-inheritance:

turtlico.lib.command module
---------------------------

.. automodule:: turtlico.lib.command
   :members:
   :undoc-members:
   :show-inheritance:

turtlico.lib.compiler module
----------------------------

.. automodule:: turtlico.lib.compiler
   :members:
   :undoc-members:
   :show-inheritance:

turtlico.lib.legacy module
--------------------------

.. automodule:: turtlico.lib.legacy
   :members:
   :undoc-members:
   :show-inheritance:

turtlico.lib.projectbuffer module
---------------------------------

.. automodule:: turtlico.lib.projectbuffer
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: turtlico.lib
   :members:
   :undoc-members:
   :show-inheritance:
