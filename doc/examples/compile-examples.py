# compile-examples.py
#
# Copyright 2021 saytamkenorh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys
import tempfile
import traceback
import py_compile

examples_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, '/usr/lib/python3.8/site-packages')
sys.path.insert(0, os.path.dirname(os.path.dirname(examples_dir)))

from gi.repository import Gio

import turtlico.lib as lib

print('--- Compile examples test ---')

walk = os.walk(examples_dir)
for root, dirs, files in walk:
    for name in files:
        if name.endswith('.tcp'):
            print(f'{name}...', end='')
            abspath = os.path.join(root, name)
            f = Gio.File.new_for_path(abspath)

            project = lib.ProjectBuffer()
            project.load_from_file(f)

            compiler = lib.Compiler(project)

            try:
                code, debug_info = compiler.compile(project.code.lines)
                with tempfile.TemporaryDirectory(prefix='turtlico') as tmpdir:
                    with open(
                            os.path.join(tmpdir, 'compile_test.py'),
                            'w+t') as file:
                        file.write(code)
                    py_compile.compile(file.name, doraise=True)
                print('OK')
            except Exception as e:
                traceback.print_exc()
                print(e)
