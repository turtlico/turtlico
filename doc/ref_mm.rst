===============
Multimedia docs
===============
.. container:: cmd
   :name: mm-ef-s

   **Create sound player for a file**

   Returns player object.

   *Parameters*

   * ``STRING`` File path

.. container:: cmd
   :name: mm-tone

   **Play tone**

   *Parameters*

   * ``NUMBER | STRING`` Frequency of the tone or tone name (C, D, E, F, G, A, B).
   * ``NUMBER`` Duration in milliseconds
   * ``BOOL`` If true then the program continues after the tone stop playing. Continuing without waiting for end is available only with the turtle plugin.
.. container:: cmd
   :name: mm-ps

   **Play sound (file or sound player)**

   *Parameters*

   * ``STRING | SOUND PLAYER`` Plays the file at the string path or unpauses the sound player.
.. container:: cmd
   :name: mm-ss

   **Pause sound player**

   *Parameters*

   * ``SOUND PLAYER`` Pauses the sound player.
.. container:: cmd
   :name: mm-ef-s-status

   **Get is player playing**

   Returns True if the sound player is not paused.

   *Parameters*

   * ``SOUND PLAYER`` Sound player.
.. container:: cmd
   :name: mm-ef-s-seek

   **Seek to given position in seconds**

   *Parameters*

   * ``SOUND PLAYER`` Sound player.
   * ``NUMBER`` New position in seconds. Must be greater or equal to zero and not greater than the duration of the media.
.. container:: cmd
   :name: mm-ef-s-pos

   **Get current position**

   Returns current position in media in seconds.

   *Parameters*

   * ``SOUND PLAYER`` Sound player.
.. container:: cmd
   :name: mm-ef-s-dur

   **Get the duration of the media opened in the player (zero if duration is unknown)**

   Returns duration of the media in seconds.

   *Parameters*

   * ``SOUND PLAYER`` Sound player.
