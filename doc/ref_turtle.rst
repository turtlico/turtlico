===========
Turtle docs
===========
.. container:: cmd
   :name: go

   **Go forward**

   Moves the turtle forward.

   *Parameters*

   * ``NUMBER`` Distance in pixels (default: 30)
.. container:: cmd
   :name: left

   **Turn left**

   Turns the turtle left.

   *Parameters*

   * ``NUMBER`` Angle in degrees (default: 90)
.. container:: cmd
   :name: right

   **Turn right**

   Turns the turtle right.

   *Parameters*

   * ``NUMBER`` Angle in degrees (default: 90)
.. container:: cmd
   :name: seth

   **Set heading angle**

   Sets the heading angle of the turtle.

   *Parameters*

   * ``NUMBER`` Angle in degrees (default: 0)
.. container:: cmd
   :name: speed

   **Speed**

   Set the speed of the turtle to an integer value (float values are rounded) in range 0..10.
   Zero represents the fastest speed (no animations).
   Otherwise greater number means faster speed.
   If input is smaller than 0.5 or greater than 10 the speed is set to zero.

   *Parameters*

   * ``NUMBER`` Speed (default: 1)
.. container:: cmd
   :name: pos

   **Set position**

   Set position of the turtle. The movement is animated.

   *Parameters*

   * ``NUMBER`` The new X coordinate (default: 0)
   * ``NUMBER`` The new Y coordinate (default: 0)
.. container:: cmd
   :name: penc

   **Set pen color**

   Set color of Turtle's pen. Without any parameters the color is set to black.

   *Parameters*

   * ``COLOR`` Pen color
   * ``COLOR`` Fill color (optional)

   *Color specification options*

   * ``STRING`` Color string, such as "red", "yellow", "#33cc8c", ...
   * ``TUPLE[r: NUMBER, g: NUMBER, b: NUMBER]`` Color tuple that represents RGB color. Each of r, g and b must be in the range 0..255.
.. container:: cmd
   :name: pu

   **Lift the pen up**

   Do not not draw when the turtle moves.

   *Parameters*

   * None
.. container:: cmd
   :name: pd

   **Put the pen down**

   Draw when the turtle moves.

   *Parameters*

   * None
.. container:: cmd
   :name: pp

   **Pen properties**

   Return or set the pen’s attributes. All the parameters are optional.

   *Parameters*

   * ``BOOL`` "shown" `See Hide turle <#ht>`_
   * ``BOOL`` "pendown" `See Lift the pen up <#pu>`_
   * ``COLOR`` "pencolor" `See Set pen color <#penc>`_
   * ``COLOR`` "fillcolor" Color that is used to fill shapes. Same format as "pencolor"
   * ``NUMBER`` "pensize" Size of line that is drawin by the turtle
   * ``NUMBER`` "speed" See `Speed`_
   * ``STRING``  "resizemode" Adaption of the turtle’s appearance. Possible values: "auto" or "user" or "noresize"
   * ``TUPLE[NUMBER, NUMBER]`` "stretchfactor" Scale of turtle's shape. "resizemode" must be set to "user".
   * ``NUMBER`` "outline" The width of the shapes’s outline. "resizemode" must be set to "user".
   * ``NUMBER`` "tilt" Rotate the turtleshape by angle from its current tilt-angle.
.. container:: cmd
   :name: ht

   **Hide turtle**

   Hides the turtle.

   *Parameters*

   * None
.. container:: cmd
   :name: st

   **Show turtle**

   Shows the turtle.

   *Parameters*

   * None
.. container:: cmd
   :name: wr

   **Write text on screen**

   *Parameters*

   * ``STRING`` Text to write
   * ``BOOL`` "move" If true, the pen is moved to the bottom-right corner of the text (optional)
   * ``STRING`` "align" Possible values: 'left', 'center', 'right'. (optional)
   * ``FONT`` "font" A `font <./ref_base.html#font>`_. (optional)
.. container:: cmd
   :name: bf

   **Begin fill**

   Starts recording a shape that can be then filled with End fill.
   Call this, draw a shape (e.g. rectangle) and then call End fill.

   .. image:: figures/square-shape-fill.png
      :align: center

   .. figure:: figures/square-shape-fill-result.gif
      :align: center

   *Parameters*

   * None
.. container:: cmd
   :name: ef

   **End fill - fill the drawn shape**

   See `Begin fill <#bf>`_.

   *Parameters*

   * None
.. container:: cmd
   :name: lscene

   **Last loaded scene**

   The name of the last scene that was loaded using the `Load scene <#scene>`_ command.

.. container:: cmd
   :name: cs

   **Clear turtle screen**

   Delete the turtle’s drawings from the screen

   *Parameters*

   * None
.. container:: cmd
   :name: screen-c

   **Get or set the color of the screen**

   *Parameters*

   * ``COLOR`` The `color <#penc>`_. Pass None to get the current color.
.. container:: cmd
   :name: screen-i

   **Get or set the background picture of the screen**

   *Parameters*

   * ``IMAGE | STRING`` The new backgroud picture. It can be a string path name or an `image <#img>`_.
.. container:: cmd
   :name: scene

   **Load scene from file**

   Loads a scene from file.
   Scene objects can be accessed using global variables with names corresponding to names of scene objects.
   If None then this clears the screen (deletes the turtles and reverts it to default state).

   *Parameters*

   * ``STRING`` Path to scene file.  (default: None)
.. container:: cmd
   :name: img

   **Image file**

   Editable object that represents an image file.
   Drag and drop an image from file explorer to your program directly or put this icon into the program and enter path manually.

.. container:: cmd
   :name: ds

   **Set turtle shape**

   Changes appearance of the turtle.

   *Parameters*

   * ``STRING`` Set turtle appearance to a predefined shape 'arrow', 'turtle', 'circle', 'square', 'triangle', 'classic'.

   *Parameters*

   ``IMAGE``  Set turtle appearance to an `image <#img>`_.
.. container:: cmd
   :name: newt

   **Create a new turtle**

   Creates a new turtle. The result of this function should be stored in a variable.
   All turtle functions can be accessed via this variable.

   .. figure:: figures/new-turtle.png
      :align: center

   Creates a new turtle and moves it forward.

   *Parameters*

   * None
.. container:: cmd
   :name: gett

   **Get the predefined turtle object**

   Returns the predefined turtle object.
   It works then just like turtle object returned by `Create a new turtle <#newt>`_.

   *Parameters*

   * None
.. container:: cmd
   :name: plcimg

   **Place image at the position of the turtle**

   *Parameters*

   * ``IMAGE`` An `image <#img>`_.
.. container:: cmd
   :name: sleep

   **Sleep n seconds**

   Sleeps n seconds.
   If "seconds" is None then the program will wait until a key is pressed or mouse is clicked.

   *Parameters*

   * ``NUMBER`` "seconds" Number of seconds to sleep. (default: None)
   * ``BOOL`` "block" Block key press events while sleeping. This has no effect if "seconds" is None. (default: True)
.. container:: cmd
   :name: tcf-collision

   **Circular collision**

   Checks for collisions between two turtles.

   *Parameters*

   * ``TURTLE`` Turtle object A.
   * ``TURTLE`` Turtle object B.
   * ``FUNCTION`` Callback. This function is called when a collision occurs.
   * ``NUMBER`` Collider size of turtle A. (default: 10)
   * ``NUMBER`` Collider size of turtle B. (default: 10)
   * ``ANY`` User data to pass to callback. (optional)

.. container:: cmd
   :name: tcf-collision-rect

   **Rectangular collision**

   Checks for collisions between two turtles.

   *Parameters*

   * ``TURTLE`` Turtle object A.
   * ``TURTLE`` Turtle object B.
   * ``FUNCTION`` Callback. This function is called when a collision occurs.
   * ``TUPLE[width: NUMBER, height: NUMBER]`` Collider size of turtle A. (default: (15, 15))
   * ``TUPLE[width: NUMBER, height: NUMBER]`` Collider size of turtle B. (default: (15, 15))
   * ``ANY`` User data to pass to callback. (optional)

.. container:: cmd
   :name: undo

   **Undo the last turtle action**

   Undoes the last turtle action.
   For example removes drawn line and moves turtle to the previous position.

   *Parameters*

   * None
.. container:: cmd
   :name: circle

   **Draw circle**

   Draws a circle with a specified radius.

   *Parameters*

   * ``NUMBER`` Radius.
   * ``NUMBER`` Extent - determines which part of the circle is drawn. (optional)
   * ``NUMBER`` Steps - the number of steps tu use. If not given, it will be calculated automatically. May be used to draw regular polygons. (optional)
.. container:: cmd
   :name: turbo

   **Turbo mode**

   Turbo mode can hide turtle actions from the user.
   This is useful for example during preparation of the scene.

   *Parameters*

   * ``NUMBER`` "turbo" There are three levels of turbo mode:

     * 0 - Off - Set default turtle speed
     * 1 - On - Set maximal turtle speed
     * 2 - Full - Set maximal turtle speed and disable screen updating (user can not see any action)
     * 3 - Do not change turtle parameters. Use this to set "do_no_render" parameter only.

     Default value: 0
   * ``BOOL`` "do_not_render" Disables screen updating. This is automatically set to True when "turbo" is level 2. (default: False)
   * ``TURTLE`` "t" Turtle to use instead of the default one (when changing turtle speed).
.. container:: cmd
   :name: screenp

   **Camera properties**

   Changes position of the "camera" and also resizes the window of the program.

   If "x" and "y" is not given then this command will just only resize the window.
   If "width" or "height" is greater than the size of the screen then scrollbars are shown. Otherwise they are hidden.

   *Parameters*

   * ``NUMBER`` "x" X coordinate of the camera (optional)
   * ``NUMBER`` "y" Y coordinate of the camera (optional)
   * ``NUMBER`` "width" Width of the program window (optional)
   * ``NUMBER`` "hegiht" Height of the program window (optional)
.. container:: cmd
   :name: ekey

   **Connect a function to handle key presses**

   You can also use the functions dialog to create a new method and connect key preess signal to it automatically.

   *Parameters*

   * ``FUNCTION`` Callback. This is called when "key" is pressed
   * ``STRING`` "key" See `Key <./ref_base.html#key>`_
.. container:: cmd
   :name: tmr

   **Call a function after n miliseconds**

   Calls a function after n miliseconds.
   You can put this command at the end of the callback.
   Call it once manually and it will be repeatedly called.

   *Parameters*

   * ``FUNCTION`` Callback. A function with no arguments
   * ``NUMBER`` Number of milliseconds to call the function after
.. container:: cmd
   :name: numi

   **Number input**

   Pops up a dialog window with a number input field.
   Returns the entered number.

   *Parameters*

   * ``STRING`` Title of the window (default: 'Number')
   * ``STRING`` Prompt text (default: 'Enter a number:')
   * ``NUMBER`` "default" Default number (optional)
   * ``NUMBER`` "minval" Minimal possible value (optional)
   * ``NUMBER`` "maxval" Maximal possible value (optional)
.. container:: cmd
   :name: stri

   **String input**

   Pops up a dialog window with a string input field.
   Returns the entered string.

   *Parameters*

   * ``STRING`` Title of the window (default: 'String')
   * ``STRING`` Prompt text (default: 'Enter a string:')
.. container:: cmd
   :name: emc

   **Connect a function to handle mouse clicks**

   You can also use the functions dialog to create a new method and connect mouse click signal to it automatically.
   If "function" is None all mouse click event bindings are removed.
   If "function" is a turtle object all mouse click event bindings are removed from it.

   *Parameters*

   * ``FUNCTION`` "function" Callback. Two number variables (x and y) are passed to this function (default: None)
   * ``NUMBER`` "btn" Number of the mouse-button. Defaults to left mouse button (default: 1)
   * ``TURTLE`` "turtle" If not None the callback will be called only if user clicked on the specified turtle (default: None)
   * ``BOOL`` "add" If True, a new binding will be added, otherwise it will replace a former binding (optional)
.. container:: cmd
   :name: xcorc28c29

   **Get x coordinate**

   Gets current x coordinate of the turtle.

.. container:: cmd
   :name: ycorc28c29

   **Get y coordinate**

   Gets current y coordinate of the turtle.

.. container:: cmd
   :name: visible

   **Is turtle visible?**

   True if turtle is visible.
