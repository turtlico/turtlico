/**
 * Copyright (C) 2021 saytamkenorh
 * 
 * This file is part of turtlico.
 * 
 * turtlico is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * turtlico is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with turtlico.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <libintl.h>
#include <locale.h>

int main (int argc, char *argv[]) {
	char* bindir = g_path_get_dirname(argv[0]);
	chdir(bindir);
	// Turtlico Python binary
	char* turtlico_bin = g_build_filename(bindir, "turtlico", NULL);
	char* localedir = g_build_filename(g_path_get_dirname(bindir), "share", "locale", NULL);

	// g_setenv("GTK_CSD", "0", TRUE);
	g_setenv("GSK_RENDERER", "cairo", TRUE);
	
	gchar *env_path = g_strconcat(bindir, ";", g_getenv("PATH"), NULL);
	g_setenv("PATH", env_path, TRUE);
	g_free(env_path);

	setlocale (LC_ALL, "");
	bindtextdomain("turtlico", localedir);
        bind_textdomain_codeset("turtlico", "UTF-8");
        textdomain("turtlico");

	wchar_t *program = Py_DecodeLocale(argv[0], NULL);
	if (program == NULL) {
		fprintf(stderr, "Fatal error: cannot decode argv[0]\n");
		exit(1);
	}
	Py_SetProgramName(program);
	Py_Initialize();

	wchar_t* py_argv[argc];
	for (int i = 0; i < argc; i++) {
		py_argv[i] = Py_DecodeLocale(argv[i], NULL);
		if (py_argv[i] == NULL) {
			fprintf(stderr, "Fatal error: cannot decode argv\n");
			exit(1);
		}
	}
        PySys_SetArgv(argc, py_argv);

	int code = PyRun_SimpleFile(fopen(turtlico_bin, "rb"), turtlico_bin);
	if (Py_FinalizeEx() < 0) {
		exit(120);
	}
	PyMem_RawFree(program);
	return code;
}
