# flake8: noqa
# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from turtlico.lib import Plugin, CommandCategory
from turtlico.lib import CommandDefinition, CommandType, CommandModule, CommandEvent
from turtlico.lib.icon import icon, text
from turtlico.locale import _

def get_plugin():
    p = Plugin(_('Console'), doc_url='ref_console.html')
    p.categories = [
        CommandCategory(p, icon('console.svg'), [
            CommandDefinition('c_print', icon('console.svg'), _('Print to console'), CommandType.METHOD, 'print', ''),
            CommandDefinition('c_input', icon('input.svg'), _('Input from console'), CommandType.METHOD, 'input', ''),
            CommandDefinition('c_print_nnl', icon('no_newline.svg'), _('Print to console without newline'), CommandType.METHOD, 'tcf_c_print_nnl', ''),
            CommandDefinition('c_stderr', icon('stderr.svg'), _('Print to stderr (option)'), CommandType.CODE_SNIPPET, 'file=sys.stderr'),
        ])
    ]
    p.modules = {
        'console': CommandModule(
            deps=(),
            code=''
        ),
        'tcf_c_print_nnl': CommandModule(
            deps=(),
            code="""def tcf_c_print_nnl(text, **kwargs):
	print(text, end='', **kwargs)"""
        )
    }
    return p