# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations
import math

from gi.repository import GObject, Gtk, Gdk, Graphene, Pango, Gsk

import turtlico.lib as lib
import turtlico.lib.icon as icon
import turtlico.utils as utils


class SingleIconWidget(Gtk.Widget):
    __gtype_name__ = 'SingleIconWidget'

    _colors: lib.icon.CommandColorScheme
    _command: lib.Command
    _drag_source: Gtk.DragSource

    def __init__(self, colors: lib.icon.CommandColorScheme):
        super().__init__()
        self._colors = colors
        self._command = None

        self.props.has_tooltip = True

        self._drag_source = Gtk.DragSource.new()
        self._drag_source.set_actions(Gdk.DragAction.COPY)
        self._drag_source.connect('prepare', self._on_drag_prepare)
        self.add_controller(self._drag_source)

    def do_snapshot(self, snapshot):
        if not self._command:
            return
        append_to_snapshot(self._command,
                           snapshot, 0, 0,
                           self, self._colors)

    def do_measure(self, orientation, for_size):
        if orientation == Gtk.Orientation.HORIZONTAL:
            return (icon.WIDTH, icon.WIDTH, -1, -1)
        else:
            return (icon.HEIGHT, icon.HEIGHT, -1, -1)

    def do_get_request_mode(self):
        return Gtk.SizeRequestMode.CONSTANT_SIZE

    def do_query_tooltip(self,
                         x, y,
                         keyboard_tooltip: bool,
                         tooltip: Gtk.Tooltip) -> bool:
        if self._command is None:
            return False
        tooltip.set_text(self._command.definition.help)
        return True

    def set_command(self, command: lib.Command):
        self._command = command
        self.queue_draw()

    def _on_drag_prepare(self,
                         source: Gtk.DragSource, x: float, y: float
                         ) -> Gdk.ContentProvider:
        commands = [[self._command]]

        return prepare_drag(source, commands, self, self._colors)


def append_block_to_snapshot(commands: lib.CodePiece,
                             snapshot: Gtk.Snapshot, tx: int, ty: int,
                             widget: Gtk.Widget,
                             colors: lib.icon.CommandColorScheme,
                             code: lib.CodeBuffer = None,
                             end_x=None,
                             start_y=0, end_y=None,
                             debug_info: dict[tuple[int, int], int] = None
                             ) -> tuple[int, int]:
    overlay_rect = Graphene.Rect()
    related_border_color = [colors[lib.icon.CommandColor.BORDER][0]] * 4
    related_border_width = [1] * 4
    related_rect = Graphene.Rect()
    related_outline = Gsk.RoundedRect()
    width = 0
    height = 0

    end_y = end_y if end_y else len(commands)
    for y, line in enumerate(commands[start_y:end_y], start_y):
        height += 1
        render_x = 0
        comment_overlay = False  # Darken icons that follows blank comment (#)

        related_block_start_x = render_x
        related_block_line = (
            debug_info.get((0, y), None) if debug_info is not None else None)

        for x, command in enumerate(line):
            xp = tx + render_x * icon.WIDTH
            yp = ty + y * icon.HEIGHT
            command_width = append_to_snapshot(
                command, snapshot, xp, yp, widget, colors, code)
            render_x += command_width
            if comment_overlay:
                overlay_rect.init(
                    xp, yp,
                    command_width * icon.WIDTH, icon.HEIGHT)
                snapshot.append_color(
                    colors[icon.CommandColor.OVERLAY_COMMENT][0],
                    overlay_rect)
            if command.definition.id == '#' and not command.data:
                comment_overlay = True

            if debug_info is not None:
                command_line_next = debug_info.get((x + 1, y), None)
                is_separator = (
                    command.definition.id == 'nl'
                    or command.definition.id == 'tab')
                if ((command_line_next is not None
                        and command_line_next != related_block_line)
                        or is_separator):
                    related_block_width = render_x - related_block_start_x
                    if is_separator:
                        related_block_width -= 1

                    if related_block_width > 0:
                        related_rect.init(
                            tx + related_block_start_x * icon.WIDTH, yp,
                            related_block_width * icon.WIDTH,
                            icon.HEIGHT)
                        related_outline.init_from_rect(related_rect, 0)
                        snapshot.append_border(
                            related_outline,
                            related_border_width, related_border_color)

                    related_block_start_x = render_x
                    if command_line_next is not None:
                        related_block_line = command_line_next

            if render_x > width:
                width = render_x
            if (end_x is not None and render_x >= end_x
                    and (related_block_start_x == render_x
                         or debug_info is None)):
                break
    return (width * icon.WIDTH, height * icon.HEIGHT)


def append_to_snapshot(cmd: lib.Command,
                       snapshot: Gtk.Snapshot, x, y,
                       widget: Gtk.Widget, colors: lib.icon.CommandColorScheme,
                       code: lib.CodeBuffer = None) -> int:
    defin: lib.CommandDefinition = cmd.definition
    bg, fg = colors[defin.color]
    # Extend the width of the icon for icons with data
    data_layout, icon_width = _create_data_layout(cmd, widget)

    area = Graphene.Rect.init(
        Graphene.Rect(), x, y, icon_width * icon.WIDTH, icon.HEIGHT)

    # Background
    snapshot.append_color(bg, area)
    # Foreground icon
    if not (defin.data_only and cmd.data):
        if icon_width > 1:
            icon_area = Graphene.Rect.init(
                Graphene.Rect(), x + (icon_width - 1) * icon.WIDTH / 2, y,
                icon.WIDTH, icon.HEIGHT)
        else:
            icon_area = area
        defin.icon.snapshot(snapshot, icon_area, fg, widget.get_scale_factor())
    # Data
    if cmd.data and defin.show_data:
        if defin.id == 'img' and code is not None:
            texture = code.get_command_data_preview(cmd)
            if texture is not None:
                t_scale = max(
                    1,
                    texture.get_width() / area.get_width(),
                    texture.get_height() / area.get_height())
                ta_width = texture.get_width() / t_scale
                ta_height = texture.get_height() / t_scale
                t_area = Graphene.Rect.init(
                    Graphene.Rect(),
                    area.get_x() + area.get_width() / 2 - ta_width / 2,
                    area.get_y() + area.get_height() / 2 - ta_height / 2,
                    ta_width, ta_height)
                snapshot.append_texture(texture, t_area)
        if defin.id == 'color':
            data_color = utils.rgba(f'rgb({cmd.data})')
            layout = widget.create_pango_layout('⬤')
            _append_layout(
                snapshot, layout, icon.FONT_NORMAL, data_color, area)
        if data_layout:
            _append_layout(
                snapshot, data_layout, None, fg, area,
                0 if defin.data_only else 5)
    return icon_width


def calc_icon_width(cmd: lib.Command,
                    widget: Gtk.Widget) -> int:
    return _create_data_layout(cmd, widget)[1]


def _create_data_layout(cmd: lib.Command,
                        widget: Gtk.Widget) -> tuple[Pango.Layout, int]:
    icon_width = 1
    layout = None
    if cmd.data and cmd.definition.show_data:
        layout = widget.create_pango_layout(cmd.data)
        layout.set_font_description(icon.FONT_SMALL)
        data_layout_width = layout.get_pixel_size()[0]
        icon_width = max(icon_width, math.ceil(data_layout_width / icon.WIDTH))
    return (layout, icon_width)


def _append_layout(snapshot: Gtk.Snapshot,
                   layout: Pango.Layout, font, color: Gdk.RGBA,
                   area: Graphene.Rect, y=0):
    if font:
        layout.set_font_description(font)
    layout.set_alignment(Pango.Alignment.CENTER)
    layout.set_width(area.size.width * Pango.SCALE)
    x = area.get_x()
    y = (area.get_y() + y
         + area.size.height / 2 - layout.get_pixel_size()[1] / 2)

    translate_required = x != 0 or y != 0
    if translate_required:
        snapshot.save()
        snapshot.translate(Graphene.Point.init(Graphene.Point(), x, y))
    snapshot.append_layout(layout, color)
    if translate_required:
        snapshot.restore()


def prepare_drag(source: Gtk.DragSource,
                 commands: lib.CodePiece,
                 widget: Gtk.Widget,
                 colors: lib.icon.CommandColorScheme,
                 code: lib.CodeBuffer = None
                 ) -> Gdk.ContentProvider:
    cp = lib.get_codepiece_content_provider(commands)

    paintable = prepare_drag_paintable(commands, widget, colors, code)
    source.set_icon(*paintable)

    return cp


def prepare_drag_paintable(commands: lib.CodePiece,
                           widget: Gtk.Widget,
                           colors: lib.icon.CommandColorScheme,
                           code: lib.CodeBuffer = None
                           ) -> tuple[Gdk.Paintable, int, int]:
    """Creates a paintable that is supposed to be used as a drag icon

    Args:
        commands (lib.CodePiece): Commands to display
        widget (Gtk.Widget): Widget that request the paintable
        colors (lib.icon.CommandColorScheme): Color scheme

    Returns:
        tuple[Gdk.Paintable, int, int]: Paintable, hot x, hot y
    """
    snapshot = Gtk.Snapshot.new()
    width, height = append_block_to_snapshot(
        commands, snapshot, 0, 0, widget, colors, code)
    size = Graphene.Size().init(width, height)
    return snapshot.to_paintable(size), icon.WIDTH / 2, icon.HEIGHT / 2


GObject.type_register(SingleIconWidget)
