# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations
import os
import abc
import inspect
from enum import Enum
from typing import Dict, Tuple

import cairo
from gi.repository import Gdk, Gtk, Rsvg, Graphene, Pango, PangoCairo

import turtlico.utils as utils

WIDTH = 50
HEIGHT = 35

FONT_NORMAL: Pango.FontDescription = Pango.FontDescription.new()
FONT_NORMAL.set_weight(Pango.Weight.BOLD)
FONT_NORMAL.set_size(15 * Pango.SCALE)

FONT_SMALL: Pango.FontDescription = Pango.FontDescription.new()
FONT_SMALL.set_family('Monospace')
FONT_SMALL.set_weight(Pango.Weight.THIN)
FONT_SMALL.set_size(9 * Pango.SCALE)


class CommandIconInterface(metaclass=abc.ABCMeta):
    """Represents a command icon (only the image in the center without background).
    The two implementations are TextTexture and SVGFileTexture.
    """
    @classmethod
    def __subclasscheck__(cls, subclass):
        return (hasattr(subclass, 'snapshot')
                and callable(subclass.snapshot))

    @abc.abstractmethod
    def snapshot(self,
                 snapshot: Gtk.Snapshot,
                 bounds: Graphene.Rect,
                 fgcolor: Gdk.RGBA, scale: float):
        raise NotImplementedError


_text_texture_pango_ctx = Gtk.Image.new().get_pango_context()


@CommandIconInterface.register
class TextTexture(CommandIconInterface):
    """Represents text only icon. Can be obtained via `text` function."""
    _text: str
    _fgcolor: Gdk.RGBA
    _cached_texture_scale: float
    _cached_texture: Gdk.Texture

    _color_white = utils.rgba('#ffffff')

    def __init__(self, text: str):
        self._text = text
        self._cached_texture = None
        self._cached_texture_scale = 0
        self._fgcolor = TextTexture._color_white

    def snapshot(self,
                 snapshot: Gtk.Snapshot,
                 bounds: Graphene.Rect,
                 fgcolor: Gdk.RGBA, scale: float):
        if (self._cached_texture_scale < scale
                or not self._fgcolor.equal(fgcolor)):
            PangoCairo.context_set_resolution(
                _text_texture_pango_ctx, 96 * scale)
            layout = Pango.Layout.new(_text_texture_pango_ctx)
            layout.set_text(self._text)
            layout.set_font_description(FONT_NORMAL)
            layout_width, layout_height = layout.get_pixel_size()

            scaled_w = int(WIDTH * scale)
            scaled_h = int(HEIGHT * scale)

            cr = cairo.ImageSurface(
                cairo.Format.ARGB32,
                scaled_w, scaled_h)
            ctx = cairo.Context(cr)
            Gdk.cairo_set_source_rgba(ctx, fgcolor)

            x = int(scaled_w / 2 - layout_width / 2)
            y = int(scaled_h / 2 - layout_height / 2)
            ctx.move_to(x, y)

            PangoCairo.show_layout(ctx, layout)

            pixbuf = Gdk.pixbuf_get_from_surface(
                cr, 0, 0, scaled_w, scaled_h)

            del self._cached_texture
            self._cached_texture = Gdk.Texture.new_for_pixbuf(pixbuf)
            self._cached_texture_scale = scale
            self._fgcolor = fgcolor

        snapshot.append_texture(self._cached_texture, bounds)


class SVGFileTexture(CommandIconInterface):
    """Represents SVG icon. Can be obtained via `icon` function."""
    _handle: Rsvg.Handle
    _cached_texture: Gdk.Texture
    _cached_texture_scale: float

    def __init__(self, path: str):
        self._handle = Rsvg.Handle.new_from_file(path)
        self._cached_texture = None
        self._cached_texture_scale = 0

    def snapshot(self,
                 snapshot: Gtk.Snapshot,
                 bounds: Graphene.Rect,
                 fgcolor: Gdk.RGBA, scale: float):
        # Drawing cached textures is in large scale faster than rendering
        # the svg over and over
        if self._cached_texture_scale < scale:
            scaled_w = int(WIDTH * scale)
            scaled_h = int(HEIGHT * scale)

            cr = cairo.ImageSurface(
                cairo.Format.ARGB32, scaled_w, scaled_h)
            ctx = cairo.Context(cr)
            rect = Rsvg.Rectangle()
            rect.x = 0
            rect.y = 0
            rect.width = scaled_w
            rect.height = scaled_h
            self._handle.render_document(ctx, rect)

            pixbuf = Gdk.pixbuf_get_from_surface(cr, 0, 0, scaled_w, scaled_h)
            self._cached_texture = Gdk.Texture.new_for_pixbuf(pixbuf)
            self._cached_texture_scale = scale

        snapshot.append_texture(self._cached_texture, bounds)


class CommandColor(Enum):
    DEFAULT = 0
    INDENTATION = 1
    COMMENT = 2
    CYCLE = 3
    KEYWORD = 4
    NUMBER = 5
    STRING = 6
    OBJECT = 7
    TYPE_CONV = 8
    OVERLAY_SELECTION = 9
    OVERLAY_COMMENT = 10
    BORDER = 11


CommandColorScheme = Dict[CommandColor, Tuple[Gdk.RGBA, Gdk.RGBA]]


def _get_default_colors(default_bg: Gdk.RGBA) -> CommandColorScheme:
    _white = utils.rgba('rgb(255, 255, 255)')
    _black = utils.rgba('rgb(0, 0, 0)')
    colors = {
        CommandColor.DEFAULT: (default_bg, _white),
        CommandColor.INDENTATION: (_black, _white),
        CommandColor.COMMENT: (utils.rgba('rgb(255, 233, 140)'),
                               _black),
        CommandColor.CYCLE: (utils.rgba('rgb(200, 0, 0)'), _white),
        CommandColor.KEYWORD: (default_bg,
                               utils.rgba('rgb(0, 0, 128)')),
        CommandColor.NUMBER: (utils.rgba('rgb(0, 0, 128)'),
                              _white),
        CommandColor.STRING: (utils.rgba('rgb(255, 220, 0)'),
                              _black),
        CommandColor.OBJECT: (utils.rgba('rgb(100, 100, 100)'),
                              _white),
        CommandColor.TYPE_CONV: (default_bg,
                                 _black),
        CommandColor.OVERLAY_SELECTION: (
            utils.rgba('rgba(0, 0, 0, 0.35)'),
            None),
        CommandColor.OVERLAY_COMMENT: (
            utils.rgba('rgba(0, 0, 0, 0.35)'),
            None),
        CommandColor.BORDER: (
            utils.rgba('rgba(204, 143, 0, 1)'),
            None),
    }
    return colors


def get_default_colors() -> CommandColorScheme:
    return _get_default_colors(utils.rgba('rgb(255, 179, 0)'))


def get_default_colors_dark() -> CommandColorScheme:
    return _get_default_colors(utils.rgba('rgb(40, 40, 40)'))


def validate_color_scheme(scheme: CommandColorScheme):
    for c in CommandColor:
        if c not in scheme:
            raise Exception(f'Color {c} is missing in the color scheme!')


def icon(icon: str) -> SVGFileTexture:
    """Creates SVGFileTexture from file path.

    Args:
        icon (str): Path to the svg file.
        It should be just the name of a file in the icons subdirectory
        of the plugin directory.

    Returns:
        SVGFileTexture: The newly created icon texture
    """
    plugin_dir = os.path.dirname(inspect.stack()[1].filename)
    path = os.path.join(plugin_dir, 'icons', icon)
    return SVGFileTexture(path)


def text(icon: str) -> TextTexture:
    """Creates TextTexture from a string.

    Args:
        icon (str): The string

    Returns:
        TextTexture: The newly created text texture
    """
    return TextTexture(icon)
