# codepiece.py
#
# Copyright 2020 saytamkenorh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations
import os
from typing import List, Tuple, Union

from gi.repository import GLib, GObject, Gtk, Gdk, Gio

import turtlico.lib as lib
from turtlico.locale import _
from .command import Command, IMAGE_EXTENSIONS

MIME_TURTLICO_PROJECT = 'application/x-turtlico-project'
MIME_TURTLICO_CODEPIECE = 'application/x-turtlico-codepiece'
MIME_TURTLICO_PROJECT_FILTER = (
    _('Turtlico project'), '.tcp', MIME_TURTLICO_PROJECT
)
COMMANDS_WITH_PREVIEW = ['img']

TcpPiece = List[Tuple[str, str]]
"""List[Tuple[str, str]]: Represents a part of Turtlico project file"""
CodePiece = List[List[Command]]
"""List[List[Command]]: Represents a piece of code with known commands"""


class UnavailableCommandException(Exception):
    command_id: str

    def __init__(self, command_id: str):
        self.command_id = command_id
        super().__init__('Command "{}" not available.'.format(command_id))


class CodePieceDrop(GObject.Object):
    tcppiece: TcpPiece

    def __init__(self, tcppiece: TcpPiece):
        super().__init__()
        self.tcppiece = tcppiece


class CodeBuffer(GObject.Object):
    HISTORY_CAPACITY = 10

    lines: CodePiece
    project: lib.ProjectBuffer
    record_history: bool
    record_history_pause: bool
    history: list[CodePiece]
    history_index: int  # Has to be negative value (from end)

    _code_data_previews: dict[str, Gdk.Texture]
    _single_line: bool

    can_undo = GObject.Property(type=bool, default=False)
    can_redo = GObject.Property(type=bool, default=False)

    @GObject.Property
    def single_line(self) -> bool:
        return self._single_line

    @GObject.Signal
    def code_changed(self):
        if self.record_history and not self.record_history_pause:
            if self.history_index != -1:
                del self.history[min(self.history_index + 1, -1):]
                self.history_index = -1
            self.history.append(clone_codepiece(self.lines))
            del self.history[:-self.HISTORY_CAPACITY]
        self.props.can_undo = self.history_index > -len(self.history)
        self.props.can_redo = self.history_index < -1

    def __init__(
            self,
            project: lib.ProjectBuffer,
            record_history=False,
            code: str = None, single_line=False):
        super().__init__()
        self.project = project
        self.record_history = record_history
        self.record_history_pause = False
        self.history = []
        self.history_index = -1
        self._single_line = single_line

        self._code_data_previews = {}
        if code is not None:
            self.load(parse_tcp(code))
        else:
            self.load(None)

        if self.project is not None:
            self.project.connect(
                'available-commands-changed',
                self._on_available_commands_changed)

    def load(self, code: TcpPiece):
        self._code_data_previews.clear()
        self.history.clear()
        if code is None:
            self.lines = []
        else:
            self.lines = load_codepiece(code, self.project)
        self.emit('code-changed')

    def save(self) -> TcpPiece:
        return save_codepiece(self.lines)

    def reload(self):
        self.load(self.save())

    def insert(self,
               commands: CodePiece,
               x: int, y: int, autoindent: bool = False):
        clen = len(commands)
        if clen == 0:
            return

        # Validates the code to insert
        for i, line in enumerate(commands):
            if i < clen - 1 and line[-1].definition.id != 'nl':
                line.append(self.project.get_command('nl', None)[0])
            linelen = len(line)
            for li, c in enumerate(line):
                if not isinstance(c, lib.Command):
                    raise Exception('Code contains invalid commands')
                if c.definition.id == 'nl' and li < linelen - 1:
                    raise Exception('Invalid code')
                if c.definition.id not in self.project.available_commands:
                    raise UnavailableCommandException(c.definition.id)

        # Single line mode (remove new lines)
        if self.props.single_line:
            sl_commands = []
            for i, line in enumerate(commands):
                for li, c in enumerate(line):
                    if c.definition.id == 'nl':
                        continue
                    sl_commands.append(c)
            commands = [sl_commands]

        # Validates the coordinates
        lineslen = len(self.lines)
        if y == lineslen:
            # Checks whether a new line can be inserted
            if (not self.props.single_line) or lineslen == 0:
                # Inserting a new line (directly under last line)
                if self.props.single_line:
                    self.lines.append([])
                elif commands[-1][-1].definition.id != 'nl':
                    self.lines.append(
                        [self.project.get_command('nl', None)[0]])
                else:
                    self.lines.append([])
            else:
                # New line can't be inserted - place the code on the last line
                y = lineslen - 1
        elif y > lineslen or x > len(self.lines[y]):
            raise Exception('Invalid coordinates')

        # Tail = Commands that are after the inserted code on the same line
        tail = self.lines[y][x:]
        del self.lines[y][x:]

        # Auto indentation
        if autoindent:
            level = self._get_indent_level(y)
            indent = [self.project.get_command('tab', None)[0]] * level
        else:
            indent = []

        # Inserts the code
        self.lines[y].extend(commands[0])
        self.lines[y + 1:y + 1] = [
            (indent + line) for line in commands[1:]
        ]

        # Adds the tail again
        if len(tail) > 0:
            y += len(commands) - 1
            if self.lines[y][-1].definition.id == 'nl':
                self.lines.insert(y + 1, indent + tail)
            else:
                self.lines[y].extend(tail)

        self.emit('code-changed')

    def delete(self, s: lib.CodePieceSelection):
        update_previews = False

        start_y = s.props.start_y
        y = start_y
        end_y = s.props.end_y
        for i in range(start_y, end_y + 1):
            linelen = len(self.lines[y])
            start = 0 if i > s.start_y else s.start_x
            end = linelen if i < s.end_y else s.end_x + 1

            if not update_previews:
                for c in self.lines[y][start:end]:
                    if c.definition.id in COMMANDS_WITH_PREVIEW:
                        update_previews = True

            if start == 0 and end == linelen:
                del self.lines[y]
                continue
            del self.lines[y][start:end]
            if i < end_y:
                y += 1

        # If the remaining code does not end with newline
        # joins the code from next line or adds newline
        if (not self.props.single_line
                and y < len(self.lines)
                and self.lines[y][-1].definition.id != "nl"):
            if y <= len(self.lines) - 2:
                self.lines[y].extend(self.lines[y + 1])
                del self.lines[y + 1]
            else:
                self.lines[y].append(self.project.get_command("nl", None)[0])

        if update_previews:
            self._clean_code_data_previews()
        self.emit('code-changed')

    def clear(self):
        self.load(None)

    def replace_command(self, cmd: Command, x: int, y: int):
        self.lines[y][x] = cmd
        self._clean_code_data_previews()
        self.emit('code-changed')

    def get_range(self, s: lib.CodePieceSelection) -> CodePiece:
        output = []
        for y in range(s.start_y, s.end_y + 1):
            start_x = 0 if y > s.start_y else s.start_x
            end_x = len(self.lines[y]) - 1 if y < s.end_y else s.end_x
            output.append(self.lines[y][start_x:(end_x + 1)])
        return output

    def get_command_data_preview(self,
                                 command: Command
                                 ) -> Union[Gdk.Texture, None]:
        if command.definition.id == 'img':
            key = command.definition.id + command.data
            if key not in self._code_data_previews.keys():
                self._generate_command_data_preview(command)
            return self._code_data_previews[key]
        return None

    def undo(self):
        if not self.props.can_undo:
            return
        self.history_index -= 1
        self._load_from_history()

    def redo(self):
        if not self.props.can_redo:
            return
        self.history_index += 1
        self._load_from_history()

    def _load_from_history(self):
        self.lines = clone_codepiece(self.history[self.history_index])
        self._clean_code_data_previews()

        self.record_history_pause = True
        self.emit('code-changed')
        self.record_history_pause = False

    def _generate_command_data_preview(self, command: Command):
        key = command.definition.id + command.data
        ext = os.path.splitext(command.data)[1]

        if ext not in IMAGE_EXTENSIONS:
            self._code_data_previews[key] = None
            return

        path = os.path.normpath(os.path.join(
            self.project.props.project_file.get_parent().get_path(),
            command.data))

        if not os.path.isfile(path):
            self._code_data_previews[key] = None
            return

        file = Gio.File.new_for_path(path)
        try:
            texture = Gdk.Texture.new_from_file(file)
        except GLib.Error:
            texture = None
        self._code_data_previews[key] = texture

    def _clean_code_data_previews(self):
        remaining_previews = set(self._code_data_previews.keys())
        for line in self.lines:
            for cmd in line:
                if not cmd.data:
                    continue
                key = cmd.definition.id + cmd.data
                try:
                    remaining_previews.remove(key)
                except KeyError:
                    pass
        for key in remaining_previews:
            del self._code_data_previews[key]

    def _get_indent_level(self, y: int) -> int:
        level = 0
        for c in self.lines[y]:
            if c.definition.id != 'tab':
                break
            level += 1
        return level

    def _on_available_commands_changed(self, project):
        self.reload()


def parse_tcp(contents: str) -> TcpPiece:
    output = []  # Contains tuples (,)

    cmd = []
    field = []
    ignore_controls = False
    for c in contents:
        if not ignore_controls:
            if c == '\n' or c == '\r':
                continue
            if c == '\\':
                ignore_controls = True
                continue
            elif c == ',':
                cmd.append(''.join(field))
                field = []
                continue
            elif c == ';':
                if len(field) > 0:
                    cmd.append(''.join(field))
                if len(cmd) < 2:
                    cmd.append('')
                output.append(tuple(cmd))
                field = []
                cmd.clear()
                continue
        ignore_controls = False
        field.append(c)
    return output


def save_tcp(contents: TcpPiece) -> str:
    output = []
    for c in contents:
        if c[1]:
            data = (c[1].
                    replace('\\', '\\\\')
                    .replace(',', '\\,')
                    .replace(';', '\\;')
                    .replace('\n', '\\\n'))
            output.append(f'{c[0]},{data};')
        else:
            output.append(f'{c[0]};')
        if c[0] == 'nl':
            output.append('\n')
    return ''.join(output)


def load_codepiece(contents: TcpPiece,
                   project: lib.ProjectBuffer,
                   ignore_errors=False) -> CodePiece:
    lines = []
    line = []
    for command in contents:
        cmd, found = project.get_command(command[0], command[1])
        if found:
            line.append(cmd)
        elif not ignore_errors:
            raise UnavailableCommandException(command[0])
        if cmd.definition.id == 'nl':
            lines.append(line)
            line = []
    if len(line) > 0:
        lines.append(line)
    return lines


def save_codepiece(contents: CodePiece) -> TcpPiece:
    output = []
    for line in contents:
        for c in line:
            data = c.data if c.data else ''
            output.append((c.definition.id, data))
    return output


def compare_codepiece(a: CodePiece, b: CodePiece) -> bool:
    if len(a) != len(b):
        return False
    for y in range(len(a)):
        if len(a[y]) != len(b[y]):
            return False
        for x in range(len(a[y])):
            cmd_a = a[y][x]
            cmd_b = b[y][x]
            if cmd_a.definition.id != cmd_b.definition.id:
                return False
            if cmd_a.data != cmd_b.data:
                return False
    return True


def clone_codepiece(commands: CodePiece) -> CodePiece:
    return [line.copy() for line in commands]


def get_codepiece_content_provider(commands: CodePiece) -> Gdk.ContentProvider:
    val = GObject.Value()
    val.init(CodePieceDrop)
    drop = CodePieceDrop(save_codepiece(commands))
    val.set_value(drop)
    return Gdk.ContentProvider.new_for_value(val)


def get_tcppiece_from_provider(
        cp: Gdk.ContentProvider) -> Union[TcpPiece, None]:
    val = GObject.Value()
    val.init(CodePieceDrop)
    try:
        cp.get_value(val)
    except GLib.Error:
        return None
    return val.get_object().tcppiece


def get_tcppiece_from_clipboard(
        widget: Gtk.Widget) -> Union[TcpPiece, None]:
    clipboard = widget.get_display().get_clipboard()
    cp = clipboard.props.content
    if cp is None:
        return None
    tcppiece = lib.get_tcppiece_from_provider(cp)

    return tcppiece


# TODO: Fix this in order tu support inter app transfers
def deserialize_bytes_finish(outs: Gio.MemoryOutputStream,
                             res: Gio.AsyncResult,
                             dsr: Gdk.ContentDeserializer):
    written = outs.splice_finish(res)
    if written < 0:
        dsr.return_error()
        return

    data = outs.steal_as_bytes()
    tcppiece = parse_tcp(str(data.get_data()))

    drop = CodePieceDrop(tcppiece)
    val = dsr.get_value()  # This returns None (bug?)
    val.set_pointer(drop)

    dsr.return_success()


def deserialize_bytes(dsr: Gdk.ContentDeserializer):
    ins = dsr.get_input_stream()

    flags = (Gio.OutputStreamSpliceFlags.CLOSE_SOURCE
             | Gio.OutputStreamSpliceFlags.CLOSE_TARGET)
    outs = Gio.MemoryOutputStream.new_resizable()
    outs.splice_async(
        ins,
        flags,
        dsr.get_priority(),
        dsr.get_cancellable(),
        deserialize_bytes_finish,
        dsr)


Gdk.content_register_deserializer(
    MIME_TURTLICO_CODEPIECE,
    CodePieceDrop,
    deserialize_bytes)
