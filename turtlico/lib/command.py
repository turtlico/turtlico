# command.py
#
# Copyright 2020 saytamkenorh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations
import os
import sys
import urllib.parse
import importlib.util
import traceback
from enum import Enum
from typing import Union, NamedTuple, Callable
from dataclasses import dataclass

from gi.repository import GObject, Gio, GLib

import turtlico.utils as utils
import turtlico.lib as lib
import turtlico.lib.icon as icon

from turtlico.locale import _


IMAGE_EXTENSIONS = ['.png', '.bmp', '.gif']


class Command(NamedTuple):
    data: str
    definition: CommandDefinition


@dataclass
class CommandModule:
    deps: set[str]
    """List of another module ids that are used by code of this module."""
    code: str
    """Python code of the module.
    There is usually a function defined.
    The name of the function must have a 'tcp_' prefix.
    """
    deps_required: bool = True
    """Set to False if the code is able to work without deps"""


class CommandEvent(NamedTuple):
    name: str
    """User friendly name of the event"""
    handler: str
    """Code in tcp format that will be inserted
    to the new handler function"""
    connector: str
    """Code in tcp format that connect the event to the new function.
    Use '{0}' to get the user chosen function name."""
    params: str
    """Handler argument default variable names separated by commas."""

    def insert_into(
            self, project: lib.ProjectBuffer, code: lib.CodeBuffer, name: str):
        cmd_def = project.get_command('def', None)[0]
        cmd_obj = project.get_command('obj', name)[0]
        cmd_block = project.get_command(':', None)[0]
        cmd_nl = project.get_command('nl', None)[0]
        cmd_tab = project.get_command('tab', None)[0]
        cmd_sep = project.get_command('sep', None)[0]

        # Handler (the actual function)
        handler_code = lib.load_codepiece(
            lib.parse_tcp(self.handler.format(name)), project)

        # Function definiton line
        handler_code.insert(0, [cmd_def, cmd_obj])
        if self.params:
            handler_code[0].append(project.get_command('(', None)[0])
            for param in self.params.split(','):
                handler_code[0].extend([
                    project.get_command('obj', param)[0], cmd_sep])
            handler_code[0].append(project.get_command(')', None)[0])
        handler_code[0].extend([cmd_block, cmd_nl])

        # Function content
        if len(handler_code) < 2:
            handler_code.append([])
        handler_code[1].insert(0, cmd_tab)
        handler_code.append([cmd_nl])
        code.insert(handler_code, 0, 0, False)

        # Connector
        if self.connector:
            connector_code = lib.load_codepiece(
                lib.parse_tcp(self.connector.format(name)), project)
            code.insert(connector_code, 0, len(code.lines), False)


class LiteralParserResult(NamedTuple):
    code: str
    """Python code that represents content of the literal"""
    required_modules: set
    """Set of modules that are in code so they should be loaded"""


class CommandType(Enum):
    INTERNAL = 0
    """Command that is processed by special code in the compiler."""
    METHOD = 1
    """Represents a function that may return a result.
    Methods take parameters in parenthesis or from next icon
    of LITERAL or LITERAL_CONST type (parenthesis are automatically inserted).
    If no parameter is specified inserts parenthesis
    with code from default_params property (see CommandDefinition).
    inserts """
    KEYWORD = 2
    """Keyword with one parameter
    (the next icon is placed on the same line in Python output)."""
    CODE_SNIPPET = 3
    """Insert Python code from function variable
    without any additional logic."""
    DIPERATOR = 4
    """Dual Input oPERATOR
    Sibling icons are placed on the same line in output along the DIPERATOR.
    Like KEYWORD but does not start a new line."""
    KEYWORD_WITH_ARGS = 5
    """Keyword with multiple parameters (commands inserted on the same line).
    Paremeters ends with :."""
    LITERAL = 6
    """Literals represent their data.
    They can be used as a paramater for a method without parenthesis.
    The command must have function property set to a function that returns
    data representation in Python code
    (see LiteralParserResult and function property in CommandDefinition)"""
    LITERAL_CONST = 7
    """Like LITERAL but has constant data (stored in function).
    The difference between this and CODE_SNIPPET is that CODE_SNIPPET can't
    be used as a parameter without parenthesis.
    """


class MissingPluginException(Exception):
    def __init__(self, plugin_id: str):
        super().__init__(_('Plugin "{}" is missing.').format(plugin_id))


class CommandDefinition(GObject.Object):
    id: str
    """str: Command id.
    ID has to be original and should contain no special characters."""
    icon: icon.CommandIconInterface
    """turtlico.lib.icon.base.CommandIconInterface: Icon of the command"""
    help: str
    """str: A short string that tells the user what does the command do"""
    color: icon.CommandColor
    """turtlico.lib.icon.base.CommandColor: Color of the command.
    See docs of CommandColor."""
    data_only: bool
    """bool: True if the original icon of the command
    vanishes if any data are set"""
    show_data: bool
    """bool: True if data are presented via a text label to the user"""
    command_type: CommandType
    """CommandType: Type of the command. See docs for CommandType."""
    function: Union[str, Callable[[str, bool], LiteralParserResult]]
    """Union[str, Callable[[str, bool], LiteralParserResult]]: For most of
    command types this is the name of
    the Python function/code that is represented by this.

    For LITERAL command type this is a reference
    to a function that takes two arguments:
    command data and a boolean value that indicates whether the command
    is placed standalone in the program.
    This means that the command is not a parameter for another funcion
    or that it's not contained
    in an IF expression etc.
    The function returns a LiteralParserResult object."""
    default_params: Union[str, None]
    """str: Python code containing default parameters.
    METHOD: If there is no parameter specified
    for the command - no parenthesis or a LITERAL/LITERAL_CONST
    command follows it - code from this variable is inserted
    after the function bewtween parenthesis.
    """
    snippet: Union[lib.TcpPiece, None]
    """TcpPiece: Code that is inserted insted of this icon
    when Autocomplete context menu option is used."""

    def __init__(self,
                 id: str, command_icon: icon.CommandIconInterface, help: str,
                 command_type: CommandType, function: str = None,
                 default_params: str = None,
                 data_only: bool = False,
                 show_data: bool = True,
                 color: icon.CommandColor = icon.CommandColor.DEFAULT,
                 snippet: str = None):
        super().__init__()

        assert isinstance(id, str)
        assert isinstance(command_icon, icon.CommandIconInterface)
        assert isinstance(help, str)
        assert isinstance(command_type, CommandType)

        if command_type == CommandType.LITERAL:
            assert isinstance(function, Callable)
        elif command_type == CommandType.INTERNAL:
            assert function is None
        else:
            assert isinstance(function, str)

        if default_params is not None:
            assert isinstance(default_params, str)
        assert isinstance(data_only, bool)
        assert isinstance(show_data, bool)
        assert isinstance(color, icon.CommandColor)
        if snippet is not None:
            assert isinstance(snippet, str)

        self.id = id
        self.icon = command_icon
        self.help = help

        self.command_type = command_type
        self.function = function
        self.default_params = default_params

        self.data_only = data_only
        self.show_data = show_data
        self.color = color
        self.snippet = lib.parse_tcp(snippet) if snippet else None

    def __str__(self):
        return self.id

    def __repr__(self):
        return f'CommandDefinition(id="{self.id}")'

    @staticmethod
    def escape_id_for_html(id: str) -> str:
        return (urllib.parse.quote(id)
                .replace('%', 'c').replace('_', '-').lower())


class CommandCategory(GObject.Object):
    plugin: Plugin
    """Plugin: The plugin that will contain the category"""
    icon: icon.CommandIconInterface
    """turtlico.lib.icon.base.CommandIconInterface: Icon for the category"""
    # This is a ListStore so it could be used as a model in GTK
    command_definitions: Gio.ListStore
    """Gio.ListStore[CommandDefinition]: objects
    of commands that the category contains."""

    def __init__(self, plugin: Plugin,
                 category_icon: icon.CommandIconInterface,
                 command_definitions: list[CommandDefinition]):
        super().__init__()
        assert isinstance(plugin, Plugin)
        assert isinstance(category_icon, icon.CommandIconInterface)
        assert isinstance(command_definitions, list)

        self.plugin = plugin
        self.icon = category_icon
        self.command_definitions = Gio.ListStore.new(CommandDefinition)
        self.command_definitions.splice(0, 0, command_definitions)


class Plugin():
    id: str
    """str: The ID of the plugin. This is set automatically."""
    name: str
    """str: The user friendly name of the plugin"""
    list_priority: int
    """int: Higher priority moves the plugin higher in icons view categories"""
    categories: list[CommandCategory]
    """list[CommandCategory]: List of categories that this contains"""
    modules: dict[str, CommandModule]
    """dict[str, CommandModule]: Dictionary of module-id module pairs
    that this contains. The id must have 'tcp_' prefix."""
    events: list[CommandEvent]
    """list[CommandEvent]: List of events that this contains"""
    doc_url: Union[str, None]
    """Union[str, None]: Contains URL with reference documentation for this.
    None means that there is no documentation available for this plugin.
    Turtlico app opens this URL with command ID tag (e.g. #go) when showing
    help for a command."""

    def __init__(self, name: str, list_priority: int = 0, doc_url: str = None):
        assert isinstance(name, str)
        assert isinstance(list_priority, int)

        self.name = name
        self.list_priority = list_priority
        self.doc_url = doc_url

        self.categories = []
        self.modules = {}
        self.events = []

    @staticmethod
    def new_from_path(path: str) -> Plugin:
        """path: Path to a Python module"""
        name = Plugin.get_id_from_path(path)
        spec = importlib.util.spec_from_file_location(name, path)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        plugin = module.get_plugin()

        assert type(plugin) is Plugin

        plugin.id = name
        return plugin

    @staticmethod
    def get_all() -> dict[str, Plugin]:
        paths = Plugin.get_paths()
        return Plugin.get_from_paths(paths)

    @staticmethod
    def get_paths() -> list[str]:
        """
        Returns paths of all available plugins
        Path is a path to a Python module
        """
        paths = []
        file_plugin_dirs = [
            os.path.join(
                os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
                'plugins'),
            os.path.join(GLib.get_user_data_dir(), 'turtlico/turtlico/plugins')
        ]
        for path in file_plugin_dirs:
            if not os.path.isdir(path):
                continue
            with os.scandir(path) as it:
                for entry in it:
                    if not entry.is_dir():
                        continue
                    plugin_dir = entry.path
                    paths.extend(Plugin.get_paths_in_path(plugin_dir))
        return paths

    @staticmethod
    def get_paths_in_path(path: str) -> list[str]:
        """Return paths of plugin files in path

        Args:
            path (str): Search for plugins in this path

        Returns:
            list[str]: Paths of Python files of the plugins
        """
        paths = []
        for root, dirs, files in os.walk(path):
            for file in files:
                if ((not file.endswith('.py'))
                        or file == '__init__.py'):
                    continue
                paths.append(os.path.join(root, file))
        return paths

    @staticmethod
    def get_from_paths(
            plugin_paths: list[str]) -> dict[str, Plugin]:
        """Load plugins from paths

        Args:
            plugin_paths (list[str]): The paths

        Returns:
            dict[str, Plugin]: ID, Plugin object pairs
            sorted by list priority and name
        """
        plugin_paths = set(plugin_paths)
        plugins = {}  # Output

        for path in plugin_paths:
            id = Plugin.get_id_from_path(path)
            try:
                plugins[id] = Plugin.new_from_path(path)
            except Exception as e:
                utils.error(f'Cannot load plugin "{id}": {e}')
                print(traceback.format_exc())
        return {key: value for key, value in sorted(
            plugins.items(),
            key=lambda i: f'{sys.maxsize - i[1].list_priority}_{i[1].name}')}

    @staticmethod
    def resolve_paths_from_ids(plugin_ids: list[str]) -> list[str]:
        """Resolves plugin IDs to plugin paths that are installed in the system.

        Args:
            plugin_ids (list[str]): The list of IDs to resolve

        Raises:
            MissingPluginException: Raises if a plugin is not found

        Returns:
            list[str]: List of paths
        """

        available_plugins = {}  # ID, path pairs
        for p in lib.Plugin.get_paths():
            available_plugins[lib.Plugin.get_id_from_path(p)] = p

        paths = []
        for plugin_id in plugin_ids:
            if plugin_id in available_plugins:
                paths.append(available_plugins[plugin_id])
            else:
                raise MissingPluginException(plugin_id)

        return paths

    @staticmethod
    def get_id_from_path(path: str) -> str:
        """
        Returns id as a string
        """
        return os.path.splitext(os.path.basename(path))[0]
