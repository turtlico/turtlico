# projectbuffer.py
#
# Copyright 2020 saytamkenorh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from typing import Union

from gi.repository import Gio, GObject

import turtlico.lib as lib
import turtlico.lib.legacy as legacy
from turtlico.locale import _

FILE_VERSION_FORMAT = 2
DEFAULT_PROJECT = [('fver', FILE_VERSION_FORMAT), ('plugin', 'turtle')]


class CorruptedFileException(Exception):
    def __init__(self):
        super().__init__(_('File syntax is corrupted'))


class RemovedUsedPluginException(Exception):
    plugin: lib.Plugin
    missing_commands: set[lib.Command]

    def __init__(self, missing_commands: set[lib.Command], plugin: lib.Plugin):
        super().__init__()
        self.missing_commands = missing_commands
        self.plugin = plugin

    def __str__(self) -> str:
        if len(self.missing_commands) > 5:
            commands = (', '.join(
                [f'"{c.definition.help}"'
                    for c in list(self.missing_commands)[:5]
                 ])
                + _(' and others'))
        else:
            commands = ', '.join(
                [f'"{c.definition.help}"' for c in self.missing_commands])
        return _('Command(s) {} from plugin "{}" are present in the program. Please remove them before disabling the plugin.').format(  # noqa: E501
            commands,
            self.plugin.name
        )


class ProjectBuffer(GObject.Object):
    """Contains information about a project"""

    __gtype_name__ = "ProjectBuffer"

    _project_file: Gio.File
    _run_in_console: bool

    available_commands: dict[str, lib.Command]
    code: lib.CodeBuffer
    enabled_plugins: dict[str, lib.Plugin]
    changed = GObject.Property(type=bool, default=False)

    @GObject.Property(type=bool, default=False)
    def run_in_console(self):
        return self._run_in_console

    @run_in_console.setter
    def run_in_console(self, value):
        if value != self._run_in_console:
            self._run_in_console = value
            self.props.changed = True

    @GObject.Property(type=Gio.File)
    def project_file(self):
        """The Gio.File of currently opened project"""
        return self._project_file

    @project_file.setter
    def project_file(self, value):
        self._project_file = value

    @GObject.Signal
    def available_commands_changed(self):
        pass

    def __init__(self):
        super().__init__()

        self._project_file = None
        self._run_in_console = False

        self.enabled_plugins = {}
        self.available_commands = {}
        self.code = lib.CodeBuffer(
            project=self, record_history=True, code=None)
        self.code.connect('code-changed', self._on_code_changed)
        self.load_from_file(file=None)

    def load_from_file(self, file: Gio.File):
        if file is not None:
            # Reads the file
            file_dis = Gio.DataInputStream.new(file.read())
            source = file_dis.read_upto('\0', 1)[0]
            file_dis.close()
            # Parses the file
            project = lib.parse_tcp(source)
        else:
            project = DEFAULT_PROJECT.copy()

        # Reset variables
        self.props.run_in_console = False
        self.props.project_file = file

        # Load project
        # project_code contains only commands (without meta-info like plugin)
        error = None
        project_code = []
        # IDs of plugins that are requested to load
        plugin_ids = set(['base'])
        file_version = 0

        for cmd in project:
            if len(cmd) != 2:
                error = CorruptedFileException()
                continue
            id = cmd[0]
            data = cmd[1]
            if id == 'plugin':
                plugin_id = lib.Plugin.get_id_from_path(data)
                plugin_ids.add(plugin_id)
            elif id == 'fver':
                file_version = int(data)
                # Skips to conversion from Turtlico 0.x projects
                if file_version <= 1:
                    break
            elif id == 'fconsole':
                self.props.run_in_console = data == 'True'
            else:
                project_code.append(cmd)

        if file_version <= 1:
            error = None
            project_code, plugins, self.props.run_in_console = (
                legacy.tcp_1_to_2(
                    source, file_version)
            )
            plugin_ids.clear()
            plugin_ids.update(plugins)

        if error is not None:
            raise error

        enabled_plugin_paths = lib.Plugin.resolve_paths_from_ids(plugin_ids)
        self.code.load(None)
        self._reload_plugins(enabled_plugin_paths)

        self.code.load(project_code)
        self.props.changed = False

    def save(self) -> bool:
        if self.props.project_file is None:
            raise Exception(
                'Project has not been saved yet. Please use save_as instead.')
        return self.save_as(self.props.project_file)

    def save_as(self, file: Gio.File) -> bool:
        """Saves buffer to the file. The file will become current project_file.

        Args:
            file (Gio.File): The file

        Returns:
            bool: True if the project was saved successfully
        """
        output = []

        # Meta-info
        output.append(('fver', str(FILE_VERSION_FORMAT)))
        for p in self.enabled_plugins:
            # Base is enabled by default
            if p == 'base':
                continue
            output.append(('plugin', p))
        output.append((('fconsole'), str(self.props.run_in_console)))

        # Commands
        output.extend(self.code.save())

        self.props.project_file = file
        outs = file.replace(None, False, Gio.FileCreateFlags.NONE)
        content = lib.save_tcp(output)
        ok, bytes_written = outs.write_all(content.encode('utf-8'))
        if ok:
            self.props.changed = False

        return ok

    def _update_available_commands(self):
        self.available_commands.clear()
        for p in self.enabled_plugins.values():
            for c in p.categories:
                for cdefin in c.command_definitions:
                    self.available_commands[cdefin.id] = lib.Command(
                        None, cdefin)
        self.emit('available_commands_changed')

    def _reload_plugins(self, plugin_paths: list[str]):
        """Loads plugin from paths and updates available commands.

        Args:
            enabled_plugins (list[str]): Plugin paths to load.
            Last plugin in the collection has the highest priority
            If there are more commands with the same id then it's used
            the one from the last plugin.
        """
        self.enabled_plugins = lib.Plugin.get_from_paths(
            plugin_paths)

        self._update_available_commands()

    def set_plugin_enabled(self, plugin_id: str, enabled: bool):
        assert isinstance(plugin_id, str)

        if enabled:
            plugin_ids = set(self.enabled_plugins.keys()).union({plugin_id})
        else:
            # Check for usage of commands contained in the plugin
            if plugin_id in self.enabled_plugins.keys():
                plugin = self.enabled_plugins[plugin_id]

                plugin_commands = []
                for category in plugin.categories:
                    for c in category.command_definitions:
                        plugin_commands.append(c)

                missing = set()
                for line in self.code.lines:
                    for c in line:
                        if c.definition in plugin_commands:
                            missing.update([c])
                if len(missing) > 0:
                    raise RemovedUsedPluginException(
                        missing, plugin
                    )
            plugin_ids = set(self.enabled_plugins.keys()) - {plugin_id}

        plugin_paths = lib.Plugin.resolve_paths_from_ids(plugin_ids)

        self._reload_plugins(plugin_paths)

    def get_command(self, id, data) -> tuple[lib.Command, bool]:
        command = self.available_commands.get(id, None)
        if command is None:
            return (None, False)
        if not data:
            return (command, True)
        return (self.set_command_data(command, data), True)

    def get_definition_plugin(self,
                              command: lib.CommandDefinition
                              ) -> Union[lib.Plugin, None]:
        for plugin in self.enabled_plugins.values():
            for c in plugin.categories:
                if command in c.command_definitions:
                    return plugin
        return None

    def set_command_data(self, command, data) -> lib.Command:
        if not data:
            return self.available_commands.get(command.definition.id, None)
        return lib.Command(data, command.definition)

    def _on_code_changed(self, buffer):
        self.props.changed = True
