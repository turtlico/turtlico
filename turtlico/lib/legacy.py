# legacy.py
#
# Copyright 2021 saytamkenorh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations
import os
import distutils.util

import turtlico.lib as lib

TCP_1_STR_MARK = str(chr(31))


def tcp_1_to_2(source: str,
               file_version: int) -> tuple[lib.TcpPiece, list[str], bool]:
    commands = list()

    for line in source.splitlines():
        ignore_controls = False  # Ignore control characters
        buffer = ''

        for char in line:
            if char == ';':
                if not ignore_controls:
                    commands.append(buffer.replace('\\n', '\n'))
                    buffer = ''
                    continue
            elif char == TCP_1_STR_MARK:
                ignore_controls = not ignore_controls
            buffer += str(char)

    project = list()
    enabled_plugins = set({'base'})
    run_in_console = False

    for cmd in commands:
        props = []
        ignore_control = False
        buffer = ''

        for c in cmd:
            if not ignore_control and c == ',':
                props.append(buffer)
                buffer = ''
                continue
            if c == TCP_1_STR_MARK:
                ignore_control = not ignore_control
            else:
                buffer += str(c)

        if len(props) != 2:
            raise lib.CorruptedFileException()

        cid = props[0]
        if len(cid) > 1 and cid[1] == '_':
            props[0] = cid[2:]

            if props[0] == 'collision':
                props[0] = 'tcf_collision'

            cid = props[0]

        if cid == 'plugin':
            if props[1].startswith('r:') or props[1].startswith('f:'):
                props[1] = props[1][2:]
            props[1] = os.path.splitext(props[1])[0]

            if props[1] == '0turtle':
                props[1] = 'turtle'

            enabled_plugins.add(props[1])
            continue

        if cid == 'fconsole':
            run_in_console = bool(distutils.util.strtobool(props[1]))
            continue

        if cid == 'fver':
            continue

        if cid == 'color' and props[1]:
            if props[1].startswith('rgb(') and props[1].endswith(')'):
                props[1] = props[1][4:-1]

        project.append(tuple(props))

    if file_version <= 0:
        enabled_plugins.add('turtle')

    return project, enabled_plugins, run_in_console
