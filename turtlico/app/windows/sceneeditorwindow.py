# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk

import turtlico.app.widgets as widgets
import turtlico.lib as lib
from turtlico.locale import _


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/sceneeditorwindow.ui')
class SceneEditorWindow(Gtk.Window):
    __gtype_name__ = 'TurtlicoSceneEditorWindow'

    _scene_browser: widgets.SceneBrowser = Gtk.Template.Child()
    _scene_view: widgets.SceneView = Gtk.Template.Child()
    _sprites_view: widgets.SpritesView = Gtk.Template.Child()
    _sprite_props: widgets.SceneSpriteProperties = Gtk.Template.Child()

    _scene_view_gesture_click: Gtk.GestureClick

    _scene_buffer: lib.SceneBuffer

    def __init__(self,
                 project: lib.ProjectBuffer,
                 parent: Gtk.Window, colors: lib.icon.CommandColorScheme):
        super().__init__()

        if project.props.project_file is None:
            raise Exception('A saved project is required for scene edtior.')

        self.set_transient_for(parent)

        self._scene_buffer = lib.SceneBuffer(project)
        self._scene_buffer.connect(
            'notify::scene-modified', self._on_scene_modified_notify)

        self._scene_browser.set_scenebuffer(self._scene_buffer)
        self._scene_browser.colors = colors

        self._scene_view.set_scenebuffer(self._scene_buffer)
        self._scene_view.colors = colors
        self._scene_view_gesture_click = Gtk.GestureClick.new()
        self._scene_view_gesture_click.connect(
            'pressed', self._scene_view_pressed)
        self._scene_view.add_controller(self._scene_view_gesture_click)

        self._sprites_view.colors = colors
        self._sprites_view.set_scenebuffer(self._scene_buffer)

        self._sprite_props.set_sceneview(self._scene_view)

        self._scene_buffer.scan_project()
        self._update_title()
        self.maximize()

    def force_close(self):
        if not self._scene_buffer.props.scene_modified:
            self.close()
            return

        def on_checked(ok):
            self.destroy()
        self._scene_browser.check_saved(on_checked, can_cancel=False)

    @Gtk.Template.Callback()
    def _on_close_request(self, win) -> bool:
        if not self._scene_buffer.props.scene_modified:
            return False

        def on_checked(ok):
            if ok:
                self._scene_buffer.props.scene_modified = False
                self.close()
        self._scene_browser.check_saved(on_checked)
        return True

    def _on_scene_modified_notify(self, obj, prop):
        self._update_title()

    def _scene_view_pressed(self, ctl, n_press, x, y):
        self._scene_browser.cancel_edits()

    def _update_title(self):
        changed = '● ' if self._scene_buffer.props.scene_modified else ''
        self.props.title = changed + _('Scene editor')
