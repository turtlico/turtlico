# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import GObject, GLib, Gio, Gtk, Gdk

import turtlico.app.widgets as widgets
import turtlico.lib as lib
import turtlico.utils as utils
from turtlico.locale import _


class CommandEventData(GObject.Object):
    event: lib.CommandEvent

    def __init__(self, event) -> None:
        super().__init__()
        self.event = event


class EventLabel(Gtk.Label):
    event: CommandEventData

    def __init__(self, event: CommandEventData) -> None:
        super().__init__()
        self.event = event
        self.props.label = event.event.name


class FunctionInfo(GObject.Object):
    name: str
    line: int
    argc: int
    """"Argument count"""

    def __init__(self, name: str, line: int, argc: int) -> None:
        super().__init__()
        self.name = name
        self.line = line
        self.argc = argc


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/functionwidget.ui')
class FunctionWidget(Gtk.Box):
    __gtype_name__ = 'TurtlicoFunctionWidget'

    _title: Gtk.Label = Gtk.Template.Child()

    info: FunctionInfo

    @GObject.Signal(arg_types=(FunctionInfo,))
    def show_definition(self, *args):
        pass

    def __init__(self, info: FunctionInfo) -> None:
        super().__init__()
        self.info = info

        self._title.props.label = info.name
        self.props.tooltip_text = _('Use function "{}" in the program').format(
            info.name)

    @Gtk.Template.Callback()
    def _on_go_def_btn_clicked(self, btn):
        self.emit('show-definition', self.info)


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/functionswindow.ui')
class FunctionsWindow(Gtk.Window):
    __gtype_name__ = 'TurtlicoFunctionsWindow'

    _stack: Gtk.Stack = Gtk.Template.Child()
    _funcs_list: Gtk.ListBox = Gtk.Template.Child()
    _event_type_list: Gtk.ListBox = Gtk.Template.Child()
    _new_func_name_entry: Gtk.Entry = Gtk.Template.Child()
    _new_func_submit_btn: Gtk.Button = Gtk.Template.Child()

    _new_func_action: Gio.SimpleAction

    _project: lib.ProjectBuffer
    _programview: widgets.ProgramView
    _events_ls: Gio.ListStore
    _funcs_ls: Gio.ListStore
    _new_func_name_entry_key_ctl: Gtk.EventControllerKey

    def __init__(self,
                 project: lib.ProjectBuffer,
                 programview: widgets.ProgramView,
                 parent: Gtk.Window):
        super().__init__()

        self._project = project
        self._programview = programview

        # Actions
        action_group = Gio.SimpleActionGroup.new()

        self._new_func_action = utils.group_add_action(
            action_group, 'new', self._on_new_func_action)
        self._new_func_submit_action = utils.group_add_action(
            action_group, 'new-submit', self._on_new_func_submit_action)

        self.insert_action_group('function', action_group)

        # Properties
        self.set_transient_for(parent)
        self._new_func_name_entry_key_ctl = Gtk.EventControllerKey()
        self._new_func_name_entry_key_ctl.connect(
            'key_pressed', self._new_func_name_entry_key_pressed)
        self._new_func_name_entry.add_controller(
            self._new_func_name_entry_key_ctl)

        self.add_shortcut(
            utils.new_shortcut('Escape', self._on_go_back))

        # Event types
        self._events_ls = Gio.ListStore.new(CommandEventData)
        for p in self._project.enabled_plugins.values():
            for event in p.events:
                self._events_ls.append(CommandEventData(event))

        def event_cmp(a, b):
            return GLib.strcmp0(a.event.name, b.event.name)
        self._events_ls.sort(event_cmp)

        no_event = lib.CommandEvent(
            name=_('No event'),
            handler='',
            connector='',
            params=''
        )
        self._events_ls.insert(0, CommandEventData(no_event))

        self._event_type_list.bind_model(
            self._events_ls, self._event_widget_create)

        # Functions
        self._funcs_ls = Gio.ListStore.new(FunctionInfo)
        self._funcs_list.bind_model(
            self._funcs_ls, self._function_widget_create)

        self._set_new_func_btn_sensitive()
        self._reload()
        self._stack.props.visible_child_name = 'index'

    def _event_widget_create(self, item: CommandEventData):
        widget = EventLabel(item)
        return widget

    def _function_widget_create(self, item: FunctionInfo):
        widget = FunctionWidget(item)
        widget.connect('show-definition', self._on_function_widget_show_def)
        return widget

    def _reload(self):
        self._funcs_ls.remove_all()
        for y, line in enumerate(self._project.code.lines):
            if len(line) < 2:
                continue
            if line[0].definition.id != 'def':
                continue
            if line[1].definition.id != 'obj':
                continue
            name = line[1].data
            argc = 0

            for x in range(2, len(line)):
                if line[x].definition.id == ':':
                    break
                if line[x].definition.id == 'obj':
                    argc += 1

            self._funcs_ls.append(FunctionInfo(name, y, argc))

    def _on_new_func_action(self, action, param):
        self._stack.props.visible_child_name = 'new_func'

    def _on_new_func_submit_action(self, action, param):
        event_row = self._event_type_list.get_selected_row()
        if event_row is None:
            return
        # Creates the function
        assert isinstance(event_row.props.child, EventLabel)
        event = event_row.props.child.event
        name = self._new_func_name_entry.props.text
        event.event.insert_into(self._project, self._project.code, name)

        self.close()

    def _new_func_name_entry_key_pressed(
            self, ctl, keyval, keycode, state: Gdk.ModifierType) -> bool:
        if keyval == Gdk.KEY_Up:
            self._event_type_list_select_next(-1)
            return True
        elif keyval == Gdk.KEY_Down:
            self._event_type_list_select_next(1)
            return True
        return False

    def _event_type_list_select_next(self, step: int = 1):
        selection = self._event_type_list.get_selected_row()
        if selection is None:
            if step > 0:
                row = self._event_type_list.get_first_child()
            else:
                row = self._event_type_list.get_last_child()
            if row is not None:
                self._event_type_list.select_row(row)
            return

        selection_widget = selection.props.child
        assert isinstance(selection_widget, EventLabel)

        found, index = self._events_ls.find(selection_widget.event)
        assert found

        index += step
        while index < 0:
            index = self._events_ls.get_n_items() + index
        while index >= self._events_ls.get_n_items():
            index = index - self._events_ls.get_n_items()

        next_row = self._event_type_list.get_row_at_index(index)
        assert next_row is not None
        self._event_type_list.select_row(next_row)

    def _on_go_back(self, action, params):
        if self._stack.props.visible_child_name == 'index':
            self.close()
        self._stack.props.visible_child_name = 'index'

    @Gtk.Template.Callback()
    def _on_go_index_btn_clicked(self, btn):
        self._stack.props.visible_child_name = 'index'

    @Gtk.Template.Callback()
    def _on_stack_visible_child_name_notify(self, obj, prop):
        child = self._stack.props.visible_child_name
        if child == 'index':
            self.props.title = _('Functions')
        elif child == 'new_func':
            self.props.title = _('New function')
            self._new_func_name_entry.grab_focus()

    @Gtk.Template.Callback()
    def _set_new_func_btn_sensitive(self, *args):
        self._new_func_submit_btn.props.sensitive = (
            self._event_type_list.get_selected_row() is not None
            and self._new_func_name_entry.props.text.strip() != ''
        )

    @Gtk.Template.Callback()
    def _on_new_func_name_entry_activate(self, entry):
        self._new_func_submit_btn.activate()

    @Gtk.Template.Callback()
    def _on_funcs_list_row_activated(self, lb, row):
        widget = row.props.child
        assert isinstance(widget, FunctionWidget)

        project = self._project
        cmd_obj = project.get_command('obj', widget.info.name)[0]
        cmd_lp = project.get_command('(', None)[0]
        cmd_sep = project.get_command('sep', None)[0]
        cmd_rp = project.get_command(')', None)[0]

        code = [[cmd_obj, cmd_lp]]
        code[0].extend([cmd_sep] * max(widget.info.argc - 1, 0))
        code[0].append(cmd_rp)

        self._programview.paste_commands(code)

        self.close()

    def _on_function_widget_show_def(self, widget, finfo: FunctionInfo):
        self._programview.props.selection = lib.CodePieceSelection(
            0, finfo.line,
            len(self._programview._codebuffer.lines[finfo.line]) - 1,
            finfo.line
        )
        self._programview.scroll_to_selection()
        self.close()
