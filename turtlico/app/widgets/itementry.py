# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import GObject, Gtk

from turtlico.locale import _


class ItemEntry(Gtk.Box):
    original_name: str

    _label: Gtk.Label
    _entry: Gtk.Entry

    @GObject.Signal(arg_types=(str,))
    def rename(self, new_name):
        pass

    @GObject.Signal
    def delete(self):
        pass

    @GObject.Signal
    def edit_cancel(self):
        pass

    def __init__(self,
                 original_name):
        super().__init__()
        self.original_name = original_name

        self._label = Gtk.Label.new(self.original_name)
        self.append(self._label)

        self._entry = Gtk.Entry.new()
        self._entry.props.visible = False
        self._entry.props.secondary_icon_name = 'emblem-ok-symbolic'
        self._entry.props.secondary_icon_tooltip_text = _('Rename')
        self._entry.props.primary_icon_name = 'user-trash-symbolic'
        self._entry.props.primary_icon_tooltip_text = _('Delete this scene')
        self._entry.connect('activate',
                            self._on_entry_activate)
        self._entry.connect('icon-press', self._on_entry_icon_press)
        self.append(self._entry)

    def edit(self):
        self._entry.props.text = self.original_name
        self._entry.props.visible = True
        self._label.props.visible = False

        self._entry.grab_focus()

    def cancel_edit(self):
        self._entry.props.visible = False
        self._label.props.visible = True
        self.emit('edit-cancel')

    def _on_entry_icon_press(self, entry, icon_pos: Gtk.EntryIconPosition):
        if icon_pos == Gtk.EntryIconPosition.SECONDARY:
            self._rename()
        elif icon_pos == Gtk.EntryIconPosition.PRIMARY:
            self._delete()

    def _on_entry_activate(self, entry):
        self._rename()

    def _rename(self):
        if self._entry.props.text.strip() == '':
            return
        self.emit('rename', self._entry.props.text)
        self.cancel_edit()

    def _delete(self):
        self.emit('delete')
