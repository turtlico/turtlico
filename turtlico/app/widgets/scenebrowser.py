# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations
from typing import Callable

from gi.repository import GObject, GLib, Gio, Gtk, Gdk

import turtlico.app.widgets as widgets
import turtlico.lib as lib
import turtlico.utils as utils
from turtlico.locale import _


class SceneBrowserItem(widgets.ItemEntry):
    scene: lib.SceneInfo

    _drag_source: Gtk.DragSource
    _scenebuffer: lib.SceneBuffer
    _colors: lib.icon.CommandColorScheme

    def __init__(self,
                 scene: lib.SceneInfo,
                 scenebuffer: lib.SceneBuffer,
                 colors: lib.icon.CommandColorScheme):
        super().__init__(scene.props.name)

        self.scene = scene
        self._scenebuffer = scenebuffer
        self._colors = colors

        self._drag_source = Gtk.DragSource.new()
        self._drag_source.set_actions(Gdk.DragAction.COPY)
        self._drag_source.connect('prepare', self._on_drag_prepare)
        self.add_controller(self._drag_source)

        self.connect('rename', self._on_rename)
        self.connect('delete', self._on_delete)

    def _on_rename(self, obj, new_name: str):
        try:
            self.scene.rename(new_name)
        except GLib.Error as e:
            dialog = Gtk.MessageDialog(
                modal=True,
                buttons=Gtk.ButtonsType.CANCEL,
                transient_for=utils.get_parent_window(self),
                text=_('Cannot rename the scene'),
                secondary_text=e.message,
                message_type=Gtk.MessageType.ERROR)

            def close(dialog, reposne):
                dialog.close()
            dialog.connect('response', close)
            dialog.show()
        self.cancel_edit()

    def _on_delete(self, obj):
        def __delete(dialog, response: Gtk.ResponseType):
            dialog.close()
            if response != Gtk.ResponseType.YES:
                return
            try:
                self.scene.delete()
                self.unparent()
            except GLib.Error as e:
                err_dialog = Gtk.MessageDialog(
                    modal=True,
                    buttons=Gtk.ButtonsType.CANCEL,
                    transient_for=utils.get_parent_window(self),
                    text=_('Cannot delete the scene'),
                    secondary_text=e.message,
                    message_type=Gtk.MessageType.ERROR)

                def close(dialog, reposne):
                    err_dialog.close()
                err_dialog.connect('response', close)
                err_dialog.show()

        dialog = Gtk.MessageDialog(
            modal=True,
            transient_for=utils.get_parent_window(self),
            text=_('Do you really want to permanently delete the scene "{}"?'
                   ).format(self.scene.name),
            message_type=Gtk.MessageType.QUESTION)
        dialog.add_buttons(
            _('Delete'), Gtk.ResponseType.YES,
            _('Cancel'), Gtk.ResponseType.CANCEL)
        delete_btn = dialog.get_widget_for_response(Gtk.ResponseType.YES)
        delete_btn.get_style_context().add_class('destructive-action')

        dialog.connect('response', __delete)
        dialog.show()

    def _on_drag_prepare(self,
                         source: Gtk.DragSource, x: float, y: float
                         ) -> Gdk.ContentProvider:
        cmd_scene, ok_scene = self._scenebuffer.project.get_command(
            'scene', None)
        cmd_str, ok_str = self._scenebuffer.project.get_command(
            'str', self.scene.name)
        if (not ok_scene) or (not ok_str):
            return None
        commands = [[cmd_scene, cmd_str]]
        return lib.icon.prepare_drag(
            source, commands, self, self._colors,
            self._scenebuffer.project.code)


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/scenebrowser.ui')
class SceneBrowser(Gtk.Box):
    __gtype_name__ = 'TurtlicoSceneBrowser'

    _list: Gtk.FlowBox = Gtk.Template.Child()
    _notifications: widgets.AppNotifications = Gtk.Template.Child()

    _shortcut_controller: Gtk.ShortcutController
    _save_action: Gio.SimpleAction

    _scenebuffer: lib.SceneBuffer
    _scenes: Gio.ListStore
    _scenes_updating: bool
    colors: lib.icon.CommandColorScheme

    def __init__(self):
        super().__init__()
        self._scenebuffer = None
        self._scenes = Gio.ListStore.new(lib.SceneInfo)
        self._scenes_updating = False

        action_group = Gio.SimpleActionGroup.new()

        self._save_action = utils.group_add_action(
            action_group, 'current-scene.save', self._on_save_action)

        self.insert_action_group('scenebrowser', action_group)

        self._shortcut_controller = Gtk.ShortcutController.new()
        self._shortcut_controller.props.scope = Gtk.ShortcutScope.GLOBAL
        self._shortcut_controller.add_shortcut(
            utils.new_action_shortcut(
                '<Ctrl>S', 'scenebrowser.current-scene.save')
        )
        self.add_controller(self._shortcut_controller)

        self._update_title()

    def set_scenebuffer(self, buffer: lib.SceneBuffer):
        if self._scenebuffer is not None:
            raise Exception('Scene buffer is already set')
        self._scenebuffer = buffer

        self._list.bind_model(
            self._scenes, self._scene_widget_create)
        self._scenebuffer.connect(
            'notify::scene-modified', self._on_scene_modified_notify)
        self._scenebuffer.connect('project-scan', self._on_project_scan)
        self._scenebuffer.connect(
            'project-scan-failed', self._on_project_scan_failed)
        self._on_project_scan()

    def _on_save_action(self, action, param):
        self._save()

    def _save(self) -> bool:
        if self._scenebuffer.scene_name is not None:
            try:
                self._scenebuffer.save_scene()
            except GLib.Error as e:
                self._notifications.add_simple(
                    _('Cannot save the scene: ') + e.message,
                    Gtk.MessageType.ERROR
                )
                return False
        return True

    def check_saved(self, callback: Callable[[bool], None], can_cancel=True):
        if ((self._scenebuffer.scene_name is None)
                or (not self._scenebuffer.props.scene_modified)):
            callback(True)
            return
        dialog = Gtk.MessageDialog(
            modal=True,
            transient_for=utils.get_parent_window(self),
            text=(_('The scene contains unsaved changed.') + '\n'
                  + _('Would you like to save the scene?')),
            secondary_text=_('Otherwise the unsaved changed will be lost.'),
            message_type=Gtk.MessageType.QUESTION)
        dialog.add_buttons(
            _('Yes'), Gtk.ResponseType.YES,
            _('No'), Gtk.ResponseType.NO)

        if can_cancel:
            dialog.add_buttons(_('Cancel'), Gtk.ResponseType.CANCEL)

        def response(dialog, _response):
            if _response == Gtk.ResponseType.YES:
                callback(self._save())
            elif _response == Gtk.ResponseType.NO:
                callback(True)
            else:
                callback(False)
            dialog.close()

        dialog.connect('response', response)
        dialog.show()

    def _scene_widget_create(self, item: lib.SceneInfo) -> Gtk.Widget:
        widget = SceneBrowserItem(item, self._scenebuffer, self.colors)
        widget.props.halign = Gtk.Align.CENTER
        return widget

    def _on_scene_modified_notify(self, obj, prop):
        self._update_title()

    def _update_title(self):
        if self._scenebuffer is not None:
            modified = self._scenebuffer.props.scene_modified
        else:
            modified = False

        self._save_action.set_enabled(modified)

    def _on_project_scan(self, *args):
        self._scenes_updating = True
        self._scenes.splice(
            0, self._scenes.get_n_items(), self._scenebuffer.scenes)
        self._scenes_updating = False
        self._select_scene(self._scenebuffer.scene_name)

    def _on_project_scan_failed(self, scenebuffer, error: GLib.Error):
        self._notifications.add_simple(error.message, Gtk.MessageType.Error)

    @Gtk.Template.Callback()
    def _on_list_child_activated(self, list, item: Gtk.FlowBoxChild):
        self._cancel_edits()
        item.props.child.edit()

    @Gtk.Template.Callback()
    def _on_list_selected_children_changed(self, list):
        if self._scenes_updating:
            return
        self._cancel_edits()
        self._notifications.clear()

        selection = self._list.get_selected_children()
        selected_scene = None
        if len(selection) > 0:
            item = selection[0].props.child
            if isinstance(item, SceneBrowserItem):
                selected_scene = item.scene.props.name

        if self._scenebuffer.scene_name != selected_scene:
            def on_checked(ok):
                if ok:
                    try:
                        self._scenebuffer.load_scene(selected_scene)
                    except GLib.Error as e:
                        self._notifications.add_simple(
                            _('Cannot load the scene: ') + e.message,
                            Gtk.MessageType.ERROR
                        )
                        self._scenebuffer.load_scene(None)
                else:
                    self._select_scene(self._scenebuffer.scene_name)
            self.check_saved(on_checked)

    @Gtk.Template.Callback()
    def _on_new_scene_btn_clicked(self, btn):
        try:
            self._scenebuffer.add_scene()
        except GLib.Error as e:
            self._notifications.add_simple(
                _('Cannot create new scene: ') + e.message,
                Gtk.MessageType.ERROR
            )

    def _select_scene(self, name: str):
        child_to_select = None
        child = self._list.get_first_child()
        while child is not None:
            if isinstance(child, Gtk.FlowBoxChild):
                item = child.props.child
                if isinstance(item, SceneBrowserItem):
                    if item.scene.props.name == name:
                        child_to_select = child
                        break
            child = child.get_next_sibling()

        if child_to_select is not None:
            self._list.select_child(child_to_select)
        else:
            self._list.unselect_all()

        self._on_list_selected_children_changed(None)

    def cancel_edits(self):
        self._cancel_edits(True)

    def _cancel_edits(self, all=False):
        selection = self._list.get_selected_children()
        child = self._list.get_first_child()

        while child is not None:
            if isinstance(child, Gtk.FlowBoxChild):
                item = child.props.child
                if isinstance(item, SceneBrowserItem):
                    if (child not in selection) or all:
                        item.cancel_edit()
            child = child.get_next_sibling()


GObject.type_register(SceneBrowser)
