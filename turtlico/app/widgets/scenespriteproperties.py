# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations
import math
from typing import Union

from gi.repository import GObject, GLib, Gtk, Gio

import turtlico.app.widgets as widgets
import turtlico.lib as lib
import turtlico.utils as utils
from turtlico.locale import _


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/scenespriteproperties.ui')
class SceneSpriteProperties(Gtk.Box):
    __gtype_name__ = 'TurtlicoSceneSpriteProperties'

    _id_entry: Gtk.Entry = Gtk.Template.Child()
    _x_spin: Gtk.SpinButton = Gtk.Template.Child()
    _y_spin: Gtk.SpinButton = Gtk.Template.Child()
    _width_spin: Gtk.SpinButton = Gtk.Template.Child()
    _height_spin: Gtk.SpinButton = Gtk.Template.Child()
    _move_up_btn: Gtk.Button = Gtk.Template.Child()
    _move_down_btn: Gtk.Button = Gtk.Template.Child()
    _delete_btn: Gtk.Button = Gtk.Template.Child()

    _delete_action: Gio.SimpleAction
    _shortcut_controller: Gtk.ShortcutController

    _sceneview: Union[widgets.SceneView, None]
    _sync_with_scene: bool

    def __init__(self) -> None:
        super().__init__()

        self._sceneview = None
        self._sync_with_scene = True

        action_group = Gio.SimpleActionGroup.new()

        self._delete_action = utils.group_add_action(
            action_group, 'sprite.delete', self._on_delete)

        self.insert_action_group('properties', action_group)

        self._shortcut_controller = Gtk.ShortcutController.new()
        self._shortcut_controller.props.scope = Gtk.ShortcutScope.GLOBAL
        self._shortcut_controller.add_shortcut(
            utils.new_action_shortcut('Delete', 'properties.sprite.delete')
        )
        self.add_controller(self._shortcut_controller)

    def set_sceneview(self, sceneview: widgets.SceneView):
        if self._sceneview is not None:
            raise Exception('Sceneview is already set.')
        self._sceneview = sceneview

        self._update()
        self._sceneview.connect(
            'notify::selection', self._on_sceneview_selection_notify)
        self._sceneview.scenebuffer.connect(
            'content-changed', self._on_scenebuffer_content_changed)

    @Gtk.Template.Callback()
    def _on_id_entry_changed(self, entry):
        if not self._check_sync_with_scene(True):
            return
        selection = self._sceneview.props.selection
        self._sceneview.scenebuffer.rename_sprite(
            selection, self._id_entry.props.text
        )

    @Gtk.Template.Callback()
    def _on_x_spin_value_changed(self, btn):
        if not self._check_sync_with_scene(True):
            return
        selection = self._sceneview.props.selection
        self._sceneview.scenebuffer.move_sprite(
            selection, self._x_spin.props.value, selection.y)

    @Gtk.Template.Callback()
    def _on_y_spin_value_changed(self, btn):
        if not self._check_sync_with_scene(True):
            return
        selection = self._sceneview.props.selection
        self._sceneview.scenebuffer.move_sprite(
            selection, selection.x, self._y_spin.props.value)

    @Gtk.Template.Callback()
    def _on_width_spin_value_changed(self, btn):
        if not self._check_sync_with_scene(False):
            return
        self._sceneview.scenebuffer.props.scene_width = (
            self._width_spin.props.value)

    @Gtk.Template.Callback()
    def _on_height_spin_value_changed(self, btn):
        if not self._check_sync_with_scene(False):
            return
        self._sceneview.scenebuffer.props.scene_height = (
            self._height_spin.props.value)

    @Gtk.Template.Callback()
    def _on_move_up_btn_clicked(self, btn):
        if not self._check_sync_with_scene(True):
            return
        selection = self._sceneview.props.selection
        self._sceneview.scenebuffer.move_sprite_up(selection)

    @Gtk.Template.Callback()
    def _on_move_down_btn_clicked(self, btn):
        if not self._check_sync_with_scene(True):
            return
        selection = self._sceneview.props.selection
        self._sceneview.scenebuffer.move_sprite_down(selection)

    def _on_delete(self, action, param):
        if not self._check_sync_with_scene(True):
            return
        selection = self._sceneview.props.selection
        self._sceneview.scenebuffer.remove_sprite(selection)

    def _check_sync_with_scene(self, wants_selection: bool) -> bool:
        if self._sceneview is None:
            return False
        if not self._sync_with_scene:
            return False
        selection = self._sceneview.props.selection
        if (selection is not None) != wants_selection:
            return False
        return True

    def _update(self):
        self._sync_with_scene = False
        self.__update()
        self._sync_with_scene = True

    def __update(self):
        # Nothing opened
        if self._sceneview is None:
            self._disable_all()
            return
        buffer: lib.SceneBuffer = self._sceneview.scenebuffer
        sprite: lib.SceneSprite = self._sceneview.props.selection
        if buffer.scene_name is None:
            self._disable_all()
            return

        # Scene properties
        if sprite is None:
            self._id_entry.props.text = _('Scene')
            self._id_entry.props.sensitive = False
            self._reset_spinbutton(self._x_spin)
            self._reset_spinbutton(self._y_spin)
            self._reset_spinbutton(
                self._width_spin, True, buffer.scene_width, step=10)
            self._reset_spinbutton(
                self._height_spin, True, buffer.scene_height, step=10)
            self._move_up_btn.props.sensitive = False
            self._move_down_btn.props.sensitive = False
            self._delete_action.set_enabled(False)
            return

        # Sprite properties
        size = self._sceneview.get_sprite_info(sprite)[0]
        width = size.get_width()
        height = size.get_height()

        min_x = math.floor(-buffer.scene_width / 2 - width / 2)
        max_x = math.ceil(buffer.scene_width / 2 + width / 2)
        min_y = math.floor(-buffer.scene_height / 2 - height / 2)
        max_y = math.ceil(buffer.scene_height / 2 + height / 2)

        if self._id_entry.props.text != sprite.id:
            self._id_entry.props.text = sprite.id
        self._id_entry.props.sensitive = True
        self._reset_spinbutton(self._x_spin, True, sprite.x, min_x, max_x)
        self._reset_spinbutton(self._y_spin, True, sprite.y, min_y, max_y)
        self._reset_spinbutton(self._width_spin, False, width)
        self._reset_spinbutton(self._height_spin, False, height)

        layer = self._sceneview.scenebuffer.scene_sprites.index(sprite)
        self._move_up_btn.props.sensitive = layer < len(
            self._sceneview.scenebuffer.scene_sprites) - 1
        self._move_down_btn.props.sensitive = layer > 0

        self._delete_action.set_enabled(True)

    def _disable_all(self):
        self._id_entry.props.text = ''
        self._id_entry.props.sensitive = False

        self._reset_spinbutton(self._x_spin)
        self._reset_spinbutton(self._y_spin)
        self._reset_spinbutton(self._width_spin)
        self._reset_spinbutton(self._height_spin)

        self._move_up_btn.props.sensitive = False
        self._move_down_btn.props.sensitive = False
        self._delete_action.set_enabled(False)

    def _reset_spinbutton(
            self, btn: Gtk.SpinButton,
            editable: bool = False, value: int = 0,
            min: int = 0, max: int = GLib.MAXINT, step: int = 1):
        btn.props.editable = editable
        btn.props.sensitive = editable
        btn.props.adjustment.configure(value, min, max, step, 50, 0)

    def _on_sceneview_selection_notify(self, obj, prop):
        self._update()

    def _on_scenebuffer_content_changed(self, buffer):
        self._update()


GObject.type_register(SceneSpriteProperties)
