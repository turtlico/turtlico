# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import GObject, Gio, Gtk, Gdk, Graphene

import turtlico.app.windows as windows
import turtlico.app.widgets as widgets
import turtlico.lib as lib
import turtlico.lib.icon as icon
from turtlico.locale import _


class CommandCategoryWidget(Gtk.Widget):
    __gtype_name__ = 'CommandCategoryWidget'

    _category: lib.CommandCategory
    _icon_bounds: Graphene.Rect

    def __init__(self, category: lib.CommandCategory):
        super().__init__()
        assert isinstance(category, lib.CommandCategory)
        self._category = category
        self._icon_bounds = Graphene.Rect().init(
            0, 0, icon.WIDTH * 0.75, icon.HEIGHT * 0.75)

        self.add_css_class('command-category-widget')

    def do_snapshot(self, snapshot):
        if not self._category:
            return

        ox = (self.get_width() - self._icon_bounds.get_width()) / 2
        oy = (self.get_height() - self._icon_bounds.get_height()) / 2
        area = Graphene.Rect().init_from_rect(self._icon_bounds).offset(ox, oy)

        self._fg_color = self.get_style_context().get_color()
        self._category.icon.snapshot(snapshot,
                                     area, self._fg_color,
                                     self.get_scale_factor())

    def do_measure(self, orientation, for_size):
        if orientation == Gtk.Orientation.HORIZONTAL:
            w = self._icon_bounds.get_width()
            return (w, w, -1, -1)
        else:
            h = self._icon_bounds.get_height()
            return (h, h, -1, -1)

    def do_get_request_mode(self):
        return Gtk.SizeRequestMode.CONSTANT_SIZE


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/iconsview.ui')
class IconsView(Gtk.Box):
    __gtype_name__ = 'TurtlicoIconsView'

    _categories_list_box: Gtk.ListBox = Gtk.Template.Child()
    _grid_view: Gtk.GridView = Gtk.Template.Child()

    sidebar_project_props_row: Gtk.ListBoxRow = Gtk.Template.Child()
    sidebar_functions_window_row: Gtk.ListBoxRow = Gtk.Template.Child()
    sidebar_scene_editor_row: Gtk.ListBoxRow = Gtk.Template.Child()

    _categories: Gio.ListStore
    _colors: lib.icon.CommandColorScheme
    _drop_target: Gtk.DropTarget
    _grid_view_factory: Gtk.SignalListItemFactory
    _project_buffer_available_commands_changed_id: int
    _project_buffer: lib.ProjectBuffer
    _scroll_ctl: Gtk.EventControllerScroll

    programview = GObject.Property(type=widgets.ProgramView)
    window = GObject.Property(type=Gtk.Window)

    @GObject.Property(type=lib.ProjectBuffer)
    def project_buffer(self):
        return self._project_buffer

    @project_buffer.setter
    def project_buffer(self, value):
        if self._project_buffer:
            self._project_buffer.disconnect(
                self._project_buffer_available_commands_changed_id)
        self._project_buffer = value
        self._project_buffer_available_commands_changed_id = (
            self._project_buffer.connect(
                'available-commands-changed', self._reload))
        self._reload()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._project_buffer = None
        self._colors = None
        self._scene_editor = None

        self._grid_view.props.has_tooltip = True

        self._grid_view_factory = Gtk.SignalListItemFactory.new()
        self._grid_view_factory.connect('bind', self._command_bind)
        self._grid_view_factory.connect('setup', self._command_setup)
        self._grid_view_factory.connect('teardown', self._command_teardown)
        self._grid_view_factory.connect('unbind', self._command_unbind)
        self._grid_view.props.factory = self._grid_view_factory

        self._categories = Gio.ListStore.new(lib.CommandCategory)
        self._categories_list_box.bind_model(self._categories,
                                             self._create_category_widget)

        # Icon trashing
        self._drop_target = Gtk.DropTarget.new(
            lib.CodePieceDrop, Gdk.DragAction.COPY | Gdk.DragAction.MOVE)
        self.add_controller(self._drop_target)

        self._scroll_ctl = Gtk.EventControllerScroll.new(
            Gtk.EventControllerScrollFlags.DISCRETE
            | Gtk.EventControllerScrollFlags.VERTICAL
        )
        self._scroll_ctl.connect('scroll', self._on_scroll)
        self.add_controller(self._scroll_ctl)

    def set_colors(self, colors: lib.icon.CommandColorScheme):
        self._colors = colors
        self._reload()

    @Gtk.Template.Callback()
    def _on_categories_list_box_row_selected(self, box, row: Gtk.ListBoxRow):
        self._grid_view.props.visible = row is not None
        if row is None:
            self._grid_view.set_model(None)
            return
        category = self._categories[row.get_index()]
        self._grid_view.set_model(
            Gtk.NoSelection.new(category.command_definitions))

    @Gtk.Template.Callback()
    def _on_sidebar_list_box_row_activated(self, box, row: Gtk.ListBoxRow):
        if row == self.sidebar_project_props_row:
            dialog = windows.ProjectPropsWindow(
                self.props.project_buffer, self.props.window)
            dialog.show()
        elif row == self.sidebar_functions_window_row:
            win = windows.FunctionsWindow(
                self.props.project_buffer, self.props.programview,
                self.props.window)
            win.show()
        elif row == self.sidebar_scene_editor_row:
            if self.props.project_buffer.props.project_file is None:
                self.props.window.notifications.clear()
                self.props.window.notifications.add_simple(
                    _('Please save the project before using scenes'))
            else:
                if self._scene_editor is not None:
                    self._scene_editor.present_with_time(Gdk.CURRENT_TIME)
                    return
                self._scene_editor = windows.SceneEditorWindow(
                    self.props.project_buffer, self.props.window,
                    self._colors)

                def clear(win):
                    self._scene_editor = None

                self._scene_editor.connect('unmap', clear)
                self._scene_editor.show()

    def _reload(self, *args):
        if not self._colors:
            raise Exception('Please call set_colors before using this widget')
        if self._project_buffer is None:
            return
        self._categories.remove_all()
        for p in self._project_buffer.enabled_plugins.values():
            for c in p.categories:
                self._categories.append(c)
        row = self._categories_list_box.get_row_at_index(0)
        if row:
            self._categories_list_box.select_row(row)

        if self._scene_editor is not None:
            self._scene_editor.force_close()
            self._scene_editor = None

    def _create_category_widget(self,
                                item: lib.CommandCategory
                                ) -> Gtk.Widget:
        widget = CommandCategoryWidget(item)
        return widget

    def _command_bind(self, factory, item: Gtk.ListItem):
        defin: lib.CommandDefinition = item.props.item
        cmd = lib.Command(None, defin)
        item.props.child.set_command(cmd)

    def _command_setup(self, factory, item: Gtk.ListItem):
        widget = icon.SingleIconWidget(self._colors)
        item.props.child = widget

    def _command_teardown(self, factory, item: Gtk.ListItem):
        pass

    def _command_unbind(self, factory, item: Gtk.ListItem):
        item.props.child.set_command(None)

    def _on_scroll(self, widget, dx: float, dy: float):
        selected_row = self._categories_list_box.get_selected_row()
        if not selected_row:
            return False
        selected_index = selected_row.get_index()
        if dy < 0:
            selected_index = selected_index - 1
            if selected_index < 0:
                selected_index = (
                    self._categories.get_n_items() + selected_index)
        elif dy > 0:
            selected_index = (
                (selected_index + 1) % self._categories.get_n_items())
        row = self._categories_list_box.get_row_at_index(
            selected_index)
        self._categories_list_box.select_row(row)
        return True


GObject.type_register(IconsView)
