# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk, Gio, GObject


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/propertyswitch.ui')
class PropertySwitch(Gtk.Box):
    __gtype_name__ = 'TurtlicoPropertySwitch'

    _key: str
    _key_settings: Gio.Settings

    _title_label: Gtk.Label = Gtk.Template.Child()
    switch: Gtk.Switch = Gtk.Template.Child()

    title = GObject.Property(type=str)

    @GObject.Property(type=str)
    def key(self):
        """GSettings key that is binded to this switch.
        """
        return self._key

    @key.setter
    def key(self, value):
        self._key = value
        self._rebind()

    @GObject.Property(type=Gio.Settings)
    def key_settings(self):
        """Gio settings that contains the key from the key property
        """
        return self._key_settings

    @key_settings.setter
    def key_settings(self, value):
        self._key_settings = value
        self._rebind()

    def __init__(self):
        super().__init__()
        self.bind_property('title', self._title_label, 'label')

    def _rebind(self):
        Gio.Settings.unbind(self.switch, 'active')
        if self.props.key_settings is None or not self.props.key:
            return
        self.props.key_settings.bind(
            self.props.key, self.switch, 'active',
            Gio.SettingsBindFlags.DEFAULT)


GObject.type_register(PropertySwitch)
