# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations
from typing import Union

from gi.repository import GObject, Gtk, Gdk, Graphene

import turtlico.lib as lib
import turtlico.utils as utils


class SceneView(Gtk.Widget, Gtk.Scrollable):
    __gtype_name__ = 'TurtlicoSceneView'

    BACKGROUND_COLOR = utils.rgba('rgb(30, 30, 30)')
    MISSING_TEXTURE_COLOR = utils.rgba('rgb(255, 0, 0)')
    SCENE_COLOR = utils.rgba('rgb(255, 255, 255)')
    SCENE_MARGIN = 30
    SELECTION_COLOR = utils.rgba('rgba(60, 60, 60, 0.5)')

    _hadjustment: Gtk.Adjustment
    _hscroll_policy: Gtk.ScrollablePolicy
    _vadjustment: Gtk.Adjustment
    _vscroll_policy: Gtk.ScrollablePolicy
    _drop_target: Gtk.DropTarget
    _drag: Gtk.GestureDrag
    _drag_source: Gtk.DragSource

    _selection: Union[lib.SceneSprite, None]
    scenebuffer: lib.SceneBuffer
    colors: lib.icon.CommandColorScheme

    _drag_sprite_offset: tuple[int, int]

    @GObject.Property(type=Gtk.ScrollablePolicy,
                      default=Gtk.ScrollablePolicy.NATURAL)
    def vscroll_policy(self):
        return self._vscroll_policy

    @vscroll_policy.setter
    def vscroll_policy(self, value):
        self._vscroll_policy = value

    @GObject.Property(type=Gtk.ScrollablePolicy,
                      default=Gtk.ScrollablePolicy.NATURAL)
    def hscroll_policy(self):
        return self._hscroll_policy

    @GObject.Property(type=Gtk.Adjustment)
    def hadjustment(self):
        return self._hadjustment

    @hadjustment.setter
    def hadjustment(self, value):
        self._hadjustment = value
        self._hadjustment.step_increment = 1
        self._hadjustment.connect('value-changed',
                                  self._on_adjustment_value_changed)
        self._update_hadjustment()

    @GObject.Property(type=Gtk.Adjustment)
    def vadjustment(self):
        return self._vadjustment

    @vadjustment.setter
    def vadjustment(self, value):
        self._vadjustment = value
        self._vadjustment.step_increment = 1
        self._vadjustment.connect('value-changed',
                                  self._on_adjustment_value_changed)
        self._update_vadjustment()

    @GObject.Property(type=lib.SceneSprite)
    def selection(self):
        return self._selection

    @selection.setter
    def selection(self, value):
        self._selection = value
        self.queue_draw()

    def __init__(self):
        super().__init__()
        self.scenebuffer = None
        self._selection = None
        self._drag_sprite_offset = None

        self._drop_target = Gtk.DropTarget.new(
            lib.CodePieceDrop, Gdk.DragAction.COPY | Gdk.DragAction.MOVE)
        self._drop_target.connect('drop', self._on_drop_target_drop)
        self.add_controller(self._drop_target)

        self._drag = Gtk.GestureDrag.new()
        self._drag.connect('drag-begin', self._on_drag_begin)
        self._drag.connect('drag-end', self._on_drag_end)
        self._drag.connect('drag-update', self._on_drag_update)
        self.add_controller(self._drag)

        self._drag_source = Gtk.DragSource.new()
        self._drag_source.props.actions = Gdk.DragAction.COPY
        self._drag_source.props.button = Gdk.BUTTON_SECONDARY
        self._drag_source.connect('prepare', self._on_source_drag_prepare)
        self.add_controller(self._drag_source)

        self._update_adjustments()

    def set_scenebuffer(self, buffer: lib.SceneBuffer):
        if self.scenebuffer is not None:
            raise Exception('Scene buffer is already set')

        self.scenebuffer = buffer
        self.scenebuffer.connect('scene-changed', self._on_scene_changed)
        self._on_scene_changed()
        self.scenebuffer.connect(
            'content-changed', self._on_scene_content_changed)
        self.scenebuffer.connect(
            'sprite-texture-changed', self._on_scene_sprite_texture_changed)
        self.scenebuffer.connect(
            'project-scan', self._on_scene_project_scan)

    def mouse_to_scene_coords(self, x: int, y: int) -> tuple[int, int]:
        x += self.props.hadjustment.props.value
        y += self.props.vadjustment.props.value

        x -= self.scenebuffer.props.scene_width / 2
        y -= self.scenebuffer.props.scene_height / 2

        return (round(x), round(-y))

    def get_sprite_at(self, x: int, y: int) -> Union[lib.SceneSprite, None]:
        """Tries to find a sprite at widget relative coordinates

        Args:
            x (int): The x coordinate
            y (int): The y coordinate

        Returns:
            Union[lib.SceneSprite, None]: The sprite or None
        """
        point = Graphene.Point().init(x, y)
        for s in reversed(self.scenebuffer.scene_sprites):
            rect = self.get_sprite_info(s)[0]
            if rect.contains_point(point):
                return s
        return None

    def _update_adjustments(self):
        self._update_hadjustment()
        self._update_vadjustment()

    def _update_hadjustment(self):
        if not self.props.hadjustment:
            self.props.hadjustment = Gtk.Adjustment.new(0, 0, 0, 0, 0, 0)
        if (self.scenebuffer is not None
                and self.scenebuffer.scene_name is not None):
            width = self.scenebuffer.props.scene_width
        else:
            width = 1

        margin = self.SCENE_MARGIN
        self.props.hadjustment.props.lower = -margin
        self.props.hadjustment.props.upper = width + margin
        self.props.hadjustment.props.page_size = min(
            width + 2 * margin, self.get_width())
        self.props.hadjustment.props.value = min(
            self.props.hadjustment.props.value,
            width - self.props.hadjustment.props.page_size
        )

    def _update_vadjustment(self):
        if not self.props.vadjustment:
            self.props.vadjustment = Gtk.Adjustment.new(0, 0, 0, 0, 0, 0)

        if (self.scenebuffer is not None
                and self.scenebuffer.scene_name is not None):
            height = self.scenebuffer.props.scene_height
        else:
            height = 1

        margin = self.SCENE_MARGIN
        self.props.vadjustment.props.lower = -margin
        self.props.vadjustment.props.upper = height + margin
        self.props.vadjustment.props.page_size = min(
            height + 2 * margin, self.get_height())
        self.props.vadjustment.props.value = min(
            self.props.vadjustment.props.value,
            height - self.props.vadjustment.props.page_size
        )

    def _on_adjustment_value_changed(self, adjustment):
        self.queue_draw()

    def _on_scene_changed(self, *args):
        self.props.selection = None
        self._update_adjustments()
        self.queue_draw()

    def _on_scene_content_changed(self, *args):
        if self.props.selection not in self.scenebuffer.scene_sprites:
            self.props.selection = None
        self.queue_draw()

    def _on_scene_sprite_texture_changed(self, *args):
        self.queue_draw()

    def _on_scene_project_scan(self, *args):
        self.queue_draw()

    def do_size_allocate(self, width: int, height: int, baseline: int):
        self._update_adjustments()

    def do_measure(self, orientation, for_size):
        if orientation == Gtk.Orientation.HORIZONTAL:
            width = abs(
                self.props.hadjustment.props.upper
                - self.props.hadjustment.props.lower)
            return (width, width, -1, -1)
        else:
            height = abs(
                self.props.vadjustment.props.upper
                - self.props.vadjustment.props.lower)
            return (height, height, -1, -1)

    def do_get_request_mode(self):
        return Gtk.SizeRequestMode.CONSTANT_SIZE

    def do_snapshot(self, snapshot: Gtk.Snapshot):
        # Scroll
        tx = -int(self.props.hadjustment.props.value)
        ty = -int(self.props.vadjustment.props.value)

        if self.scenebuffer is None or self.scenebuffer.scene_name is None:
            snapshot.render_background(
                self.get_style_context(), 0, 0,
                self.get_width(), self.get_height())
            return

        area = Graphene.Rect().init(0, 0, self.get_width(), self.get_height())

        snapshot.append_color(self.BACKGROUND_COLOR, area)

        snapshot.push_clip(area)

        # Background
        scene_area = Graphene.Rect().init(
            tx, ty,
            self.scenebuffer.props.scene_width,
            self.scenebuffer.props.scene_height)
        snapshot.append_color(self.SCENE_COLOR, scene_area)
        # Sprites
        for s in self.scenebuffer.scene_sprites:
            sprite_area, texture = self.get_sprite_info(s)
            if texture is not None:
                snapshot.append_texture(texture, sprite_area)
            else:
                snapshot.append_color(self.MISSING_TEXTURE_COLOR, sprite_area)
        # Selection
        if self.props.selection is not None:
            selection_area = self.get_sprite_info(self.props.selection)[0]
            snapshot.append_color(self.SELECTION_COLOR, selection_area)

        snapshot.pop()

        snapshot.render_focus(self.get_style_context(), 0, 0,
                              self.get_width(), self.get_height())

    def get_sprite_info(self, s: lib.SceneSprite
                        ) -> tuple[Graphene.Rect, Union[Gdk.Texture, None]]:
        """Returns widget relative area and texture of a sprite

        Args:
            s (lib.SceneSprite): The sprite

        Returns:
            tuple[Graphene.Rect, Gdk.Texture]: The results
        """
        sinfo = self.scenebuffer.sprites.get(s.name, None)
        if sinfo is not None and sinfo.texture is not None:
            width = sinfo.props.texture.get_width()
            height = sinfo.props.texture.get_height()
            texture = sinfo.texture
        else:
            width = 10
            height = 10
            texture = None

        tx = -self.props.hadjustment.props.value
        ty = -self.props.vadjustment.props.value

        return (Graphene.Rect().init(
            round(s.x - width / 2
                  + self.scenebuffer.props.scene_width / 2 + tx),
            round(-s.y - height / 2
                  + self.scenebuffer.props.scene_height / 2 + ty),
            width, height), texture)

    def _on_drop_target_drop(self,
                             dt: Gtk.DropTarget,
                             drop: lib.CodePieceDrop, x, y
                             ):
        if not self.scenebuffer or self.scenebuffer.scene_name is None:
            return False

        commands = lib.load_codepiece(
            drop.tcppiece, self.scenebuffer.project)

        # Inserts the sprite
        if len(commands) < 1 or len(commands[0]) < 1:
            return False
        cmd = commands[0][0]

        if cmd.definition.id != 'img':
            return False

        sx, sy = self.mouse_to_scene_coords(x, y)
        self.scenebuffer.add_sprite(cmd.data, sx, sy)

        return True

    def _on_drag_begin(self, ctl, start_x: float, start_y: float):
        if not self.scenebuffer or self.scenebuffer.scene_name is None:
            return

        self.props.selection = self.get_sprite_at(start_x, start_y)
        if self.props.selection is not None:
            mx, my = self.mouse_to_scene_coords(start_x, start_y)
            self._drag_sprite_offset = (
                self.props.selection.x - mx,
                self.props.selection.y - my,
            )

    def _on_drag_end(self, ctl, offset_x: float, offset_y: float):
        pass

    def _on_drag_update(self, ctl, offset_x: float, offset_y: float):
        if not self.scenebuffer or self.scenebuffer.scene_name is None:
            return
        if self.props.selection is None:
            return
        if abs(offset_x) < 5 and abs(offset_y) < 5:
            return

        ok, sx, sy = self._drag.get_start_point()
        if not ok:
            return
        x, y = self.mouse_to_scene_coords(sx + offset_x, sy + offset_y)
        ox, oy = self._drag_sprite_offset
        self.scenebuffer.move_sprite(
            self.props.selection, x + ox, y + oy)

    def _on_source_drag_prepare(
            self, source: Gtk.DragSource,
            x: float, y: float) -> Gdk.ContentProvider:
        sprite = self.get_sprite_at(x, y)
        if sprite is None:
            return None

        cmd, ok = self.scenebuffer.project.get_command(
            'obj', sprite.id)
        commands = [[cmd]]
        return lib.icon.prepare_drag(
            source, commands, self, self.colors,
            self.scenebuffer.project.code)


GObject.type_register(SceneView)
