# cli.py
#
# Copyright 2021 saytamkenorh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from gi.repository import GLib, Gio

import turtlico.utils as utils

from turtlico.locale import _


def compile(input_file: Gio.File, output_file_path: str) -> int:
    if input_file is None:
        utils.error(_('No input file specified'))
        return 1

    output_file = Gio.File.new_for_commandline_arg(output_file_path)

    import turtlico.lib as lib

    project = lib.ProjectBuffer()
    project.load_from_file(input_file)

    compiler = lib.Compiler(project)

    code, debug_info = compiler.compile(project.code.lines)

    try:
        if output_file.query_exists():
            if not output_file.delete():
                utils.error(_('Failed to overwrite output file'))
        outs = output_file.create(Gio.FileCreateFlags.NONE, None)

        outs.write_all(code.encode(), None)

        outs.close()
    except GLib.Error as e:
        utils.error(_('Cannot save output file: {}').format(e))

    return 0
